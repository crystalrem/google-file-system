package master

import (
	//"fmt"
	log "github.com/Sirupsen/logrus"
	"net"
	"net/rpc"
	"time"

	"gfs"
	"errors"
	//"fmt"
	"gfs/util"
	"sync"
)

// Master Server struct
type Master struct {
	sync.RWMutex
	line	   sync.RWMutex
	address    gfs.ServerAddress // master server address
	serverRoot string            // path to metadata storage
	l          net.Listener
	shutdown   chan struct{}
	dead       bool
	nm  *namespaceManager
	cm  *chunkManager
	csm *chunkServerManager
}

// NewAndServe starts a master and returns the pointer to it.
func NewAndServe(address gfs.ServerAddress, serverRoot string) *Master {

	m :=  &Master{
		address:    address,
		serverRoot: serverRoot,
		shutdown:   make(chan struct{}),
	}
	
	m.nm,_ = newNamespaceManager(serverRoot)
	m.cm,_ = newChunkManager(serverRoot, address)
	m.csm = newChunkServerManager(serverRoot, address)
	
	rpcs := rpc.NewServer()
	rpcs.Register(m)
	l, e := net.Listen("tcp", string(m.address))
	if e != nil {
		log.Warningf("listen error:", e)
		//log.Exit(1)
	}
	m.l = l
	
	// RPC Handler
	go func() {
		for {
			select {
			case <-m.shutdown:
				return
			default:
			}
			conn, err := m.l.Accept()
			if err == nil {
				go func() {
					rpcs.ServeConn(conn)
					conn.Close()
				}()
			} else {
				log.Warningf("accept error:", err)
				//log.Exit(1)
			}
		}
	}()
	
	// Background Task
	go func() {
		ticker := time.Tick(gfs.BackgroundInterval)
		for {
			select {
			case <-m.shutdown:
				return
			default:
			}
			<-ticker

			err := m.BackgroundActivity()
			if err != nil {
				log.Warningf("Background error ", err)
			}
		}
	}()
	
	log.Infof("Master is running now. addr = %v, root path = %v", address, serverRoot)

	return m
}

// Shutdown shuts down master
func (m *Master) Shutdown() {
	m.Lock()
	defer m.Unlock()
	m.nm.Preserve(m.serverRoot)
	m.cm.Preserve(m.serverRoot)
	//m.csm.Preserve(m.serverRoot, m.address)
	if !m.dead {
		log.Warningf("Master shuts down", m.address)
		m.dead = true
		close(m.shutdown)
		m.l.Close()
	}
}

// BackgroundActivity does all the background activities:
// dead chunkserver handling, garbage collection, stale replica detection, etc

//func (csm *chunkServerManager) RemoveServer(addr gfs.ServerAddress) (handles []gfs.ChunkHandle, err error)
//func (csm *chunkServerManager) DetectDeadServers() []gfs.ServerAddress.
/*
type chunkManager struct {
	sync.RWMutex

	chunk map[gfs.ChunkHandle]*chunkInfo
	file  map[gfs.Path]*fileInfo

	numChunkHandle gfs.ChunkHandle
}
*/
//func (csm *chunkServerManager) ChooseReReplication(handle gfs.ChunkHandle) (from, to gfs.ServerAddress, err error)
//func (cm *chunkManager) RegisterReplica(handle gfs.ChunkHandle, addr gfs.ServerAddress) error
//func (csm *chunkServerManager) AddChunk(addrs []gfs.ServerAddress, handle gfs.ChunkHandle) error
func (m *Master) BackgroundActivity() error {//checked
	m.Lock()
	defer m.Unlock()
	m.line.Lock()
	m.line.Unlock()
	serverAddressArray := m.csm.DetectDeadServers()
	chunkHandleArray := make([]gfs.ChunkHandle, 0, 1)
	for i := 0; i < len(serverAddressArray); i++ {
		tmp, _ := m.csm.RemoveServer(serverAddressArray[i])
		//fmt.Println("HHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHH\n")
		for j := 0; j < len(tmp); j++ {
			err := m.cm.DeleteChunk(tmp[j], serverAddressArray[i])
			if err != nil {
				chunkHandleArray = append(chunkHandleArray[0:], tmp[j])
			}//_, to, err2 := m.csm.ChooseReReplication(chunkHandleArray[j])
		}
	}
	for j := 0; j < len(chunkHandleArray); j++ {
			from, to, err2 := m.csm.ChooseReReplication(chunkHandleArray[j])
			if err2 != nil {
				if err2.Error() == "func (csm *chunkServerManager) ChooseReReplication(handle gfs.ChunkHandle) (from, to gfs.ServerAddress, err error):\nThe ChunkServer who doesn't have this handle doesn't exist"{
					continue
				}
				continue
				//return err2//errors.New("func (m *Master) BackgroundActivity() error:\nChooseReReplication Error")
			}
			err3 := m.cm.RegisterReplica(chunkHandleArray[j], from, to)
			if err3 != nil {
				return errors.New("func (m *Master) BackgroundActivity() error:\nRegisterReplica Error")
			}
			array := make([]gfs.ServerAddress, 0, 1)
			array = append(array, to)
			err4:= m.csm.AddChunk(array, chunkHandleArray[j]) 
			if err4 != nil {
				return errors.New("func (m *Master) BackgroundActivity() error:\nAddChunk")
			}
	}
	//fmt.Println("BackgroundActivity return")
	return nil

}

// RPCHeartbeat is called by chunkserver to let the master know that a chunkserver is alive.
// Lease extension request is included.

/*type HeartbeatArg struct {
	Address         ServerAddress // chunkserver address
	LeaseExtensions []ChunkHandle // leases to be extended
}
type HeartbeatReply struct{}*/
//func (csm *chunkServerManager) Heartbeat(addr gfs.ServerAddress) 
//func (cm *chunkManager) ExtendLease(handle gfs.ChunkHandle, primary gfs.ServerAddress) error
/*type GetPrimaryAndSecondariesArg struct {
	Handle ChunkHandle
}
type GetPrimaryAndSecondariesReply struct {
	Primary     ServerAddress
	Expire      time.Time
	Secondaries []ServerAddress
}*/
/*
type ReportSelfArg struct {
}

type ReportSelfReply struct {
	HandleList   []ChunkHandle
	ChunkVersion map[ChunkHandle]ChunkVersion
}
*/
//func (cm *chunkManager) AddChunks(handles []gfs.ChunkHandle, versions []gfs.ChunkVersion, addr gfs.ServerAddress)
func (m *Master) RPCHeartbeat(args gfs.HeartbeatArg, reply *gfs.HeartbeatReply) error {//checked
	m.Lock()
	defer m.Unlock()
	var ret error
	ret = nil
	exist := m.csm.Heartbeat(args.Address)
	if !exist {
		//fmt.Println("address = ", string(args.Address))
		var args0 gfs.ReportSelfArg
		var reply0 gfs.ReportSelfReply
		reply0.HandleList = make([]gfs.ChunkHandle, 0, 1)
		reply0.ChunkVersion = make([]gfs.ChunkVersion, 0, 1)
		util.Call(args.Address, "ChunkServer.RPCReportSelf", args0, &reply0)
		//fmt.Println("~~~~~~~~~~~~~~~~~ Length = ", len(reply0.HandleList))
		chunkHandles, _ := m.cm.AddChunks(reply0.HandleList, reply0.ChunkVersion,args.Address)
		_ = m.csm.UpdateChunkServer(args.Address, chunkHandles)
	}
	//if err1 != nil {}
	//if err0 != nil {}
	
		//Call REPORT
		//chunkHandles[]

	for i := 0; i < len(args.LeaseExtensions); i++ {
		var args1 gfs.GetPrimaryAndSecondariesArg
		args1.Handle = args.LeaseExtensions[i]
		var reply1 gfs.GetPrimaryAndSecondariesReply
		m.Unlock()
		m.RPCGetPrimaryAndSecondaries(args1, &reply1)
		m.Lock()
	}
	//fmt.Println("HeartBeat return")
	if ret != nil {
		return errors.New(
				"func (m *Master) RPCHeartbeat(args gfs.HeartbeatArg, reply *gfs.HeartbeatReply) error:\nExtendLease error")
	} else {
		return nil
	}
}


// RPCGetPrimaryAndSecondaries returns lease holder and secondaries of a chunk.
// If no one holds the lease currently, grant one.
/*type GetPrimaryAndSecondariesArg struct {
	Handle ChunkHandle
}
type GetPrimaryAndSecondariesReply struct {
	Primary     ServerAddress
	Expire      time.Time
	Secondaries []ServerAddress
}*/
//func (cm *chunkManager) GetLeaseHolder(handle gfs.ChunkHandle) (*lease, error)
/*type lease struct {
	primary     gfs.ServerAddress
	expire      time.Time
	secondaries []gfs.ServerAddress
}*/
func (m *Master) RPCGetPrimaryAndSecondaries(args gfs.GetPrimaryAndSecondariesArg, reply *gfs.GetPrimaryAndSecondariesReply) error {//checked
	m.Lock()
	m.Unlock()
	getLease, ok := m.cm.GetLeaseHolder(args.Handle)
	if ok != nil {
		return errors.New(
			"func (m *Master) RPCGetPrimaryAndSecondaries(args gfs.GetPrimaryAndSecondariesArg, reply *gfs.GetPrimaryAndSecondariesReply) error:\nGetLeaseHolder error")
	}
	reply.Primary = getLease.primary
	reply.Expire = getLease.expire
	reply.Secondaries = make([]gfs.ServerAddress, 0, 1)
	for i := 0; i < len(getLease.secondaries); i++ {
		reply.Secondaries = append(reply.Secondaries[0:], getLease.secondaries[i])
	}
	//fmt.Println("RPC Primary = ", reply.Primary, "Secondaries = ", reply.Secondaries)
	return nil
}

// RPCGetReplicas is called by client to find all chunkservers that hold the chunk.

//func (cm *chunkManager) GetReplicas(handle gfs.ChunkHandle) (*util.ArraySet, error)
/*type GetReplicasArg struct {
	Handle ChunkHandle
}
type GetReplicasReply struct {
	Locations []ServerAddress
}*/
//func (s *ArraySet) GetAll()
func (m *Master) RPCGetReplicas(args gfs.GetReplicasArg, reply *gfs.GetReplicasReply) error {//checked
	m.RLock()
	defer m.RUnlock()
	array, err := m.cm.GetReplicas(args.Handle)
	var tmp = array.GetAll()
	reply.Locations = make([]gfs.ServerAddress, 0, 1)
	for i := 0; i < len(tmp); i++ {
		reply.Locations = append(reply.Locations[0:], tmp[i].(gfs.ServerAddress))
	}
	if err != nil {
		return errors.New(
			"nc (m *Master) RPCGetReplicas(args gfs.GetReplicasArg, reply *gfs.GetReplicasReply) error:\nGetReplicas error")
	}
	//fmt.Println("RPC GetReplicas =", reply.Locations)
	return nil
}

// RPCCreateFile is called by client to create a new file

//func (nm *namespaceManager) Create(p gfs.Path) error 
/*type CreateFileArg struct {
	Path Path
}
type CreateFileReply struct{}*/
//func (csm *chunkServerManager) ChooseServers(num int) ([]gfs.ServerAddress, error)
//func (cm *chunkManager) CreateChunk(path gfs.Path, addrs []gfs.ServerAddress) (gfs.ChunkHandle, error)
func (m *Master) RPCCreateFile(args gfs.CreateFileArg, replay *gfs.CreateFileReply) error {//checked
	m.Lock()
	defer m.Unlock()
	//fmt.Println("::::::::::::::::::::::::::::::::::::::::::::::")
	err := m.nm.Create(args.Path, 0)
	if err != nil {
		return errors.New(
			"func (m *Master) RPCCreateFile(args gfs.CreateFileArg, replay *gfs.CreateFileReply) error:\nCreate error")
	}
	array, err2 := m.csm.ChooseServers(gfs.DefaultNumReplicas)
	//fmt.Println("-------------------------------------------")
	for i := 0; i < len(array); i++ {
		//fmt.Println("Servers = ", string(array[i]))
	}
	//fmt.Println("-------------------------------------------")
	if err2 != nil {
		return errors.New(
			"func (m *Master) RPCCreateFile(args gfs.CreateFileArg, replay *gfs.CreateFileReply) error:\nChooseServers error")
	}
	handle, err3 := m.cm.CreateChunk(args.Path, array)
	//func (csm *chunkServerManager) AddChunk(addrs []gfs.ServerAddress, handle gfs.ChunkHandle) error

	if err3 != nil {
		return errors.New(
			"func (m *Master) RPCCreateFile(args gfs.CreateFileArg, replay *gfs.CreateFileReply) error:\nCreate Chunk error")
	}
	err4 := m.csm.AddChunk(array, handle)
	if err4 != nil {
		return errors.New(
			"func (m *Master) RPCCreateFile(args gfs.CreateFileArg, replay *gfs.CreateFileReply) error:\nAddChunk error")
	} 
	return nil

}

// RPCMkdir is called by client to make a new directory
/*type MkdirArg struct {
	Path Path
}
type MkdirReply struct{}*/
//func (nm *namespaceManager) Mkdir(p gfs.Path) error
func (m *Master) RPCMkdir(args gfs.MkdirArg, replay *gfs.MkdirReply) error {//checked
	m.Lock()
	defer m.Unlock()
	err := m.nm.Mkdir(args.Path, 0)
	if err != nil {
		//fmt.Println("error\n");
		return errors.New(
			"func (m *Master) RPCMkdir(args gfs.MkdirArg, replay *gfs.MkdirReply) error:\nMkdir error")
	}
	return nil
}

// RPCGetFileInfo is called by client to get file information
/*type GetFileInfoArg struct {
	Path Path
}
type GetFileInfoReply struct {
	IsDir  bool
	Length int64
	Chunks int64
}*/
//func (nm *namespaceManager) FileInfo(p gfs.Path) (bool, int64, int64,error)
/*func (m *Master) RPCGetFileInfo(args gfs.GetFileInfoArg, reply *gfs.GetFileInfoReply) error {
	//m.RLock()
	//defer m.RUnlock()
	var err error 
	reply.IsDir, reply.Chunks, reply.Length, err = m.nm.FileInfo(args.Path)
	if err != nil {
		return errors.New(
			"func (m *Master) RPCGetFileInfo(args gfs.GetFileInfoArg, reply *gfs.GetFileInfoReply) error:\nFileInfo error")
	}
	return nil
}*/

// RPCGetChunkHandle returns the chunk handle of (path, index).
// If the requested index is bigger than the number of chunks of this path by exactly one, create one.

/*type GetChunkHandleArg struct {
	Path  Path
	Index ChunkIndex
}
type GetChunkHandleReply struct {
	Handle ChunkHandle
}*/

//func (cm *chunkManager) GetChunk(path gfs.Path, index gfs.ChunkIndex) (gfs.ChunkHandle, error)
//func (cs *ChunkServer) RPCCreateChunk(args gfs.CreateChunkArg, reply *gfs.CreateChunkReply) error 
func (m *Master) RPCGetChunkHandle(args gfs.GetChunkHandleArg, reply *gfs.GetChunkHandleReply) error {//checked
	m.Lock()
	defer m.Unlock()
	//fmt.Println("***master get chunk handle :path & index = ", args.Path, args.Index)
	handle, err := m.cm.GetChunk(args.Path, args.Index)
	if err != nil && err.Error() == errors.New("func (cm *chunkManager) GetChunk(path gfs.Path, index gfs.ChunkIndex) (gfs.ChunkHandle, error):\nThe index equals to the number of all the chunks plus one").Error(){
		//func (csm *chunkServerManager) ChooseServers(num int) ([]gfs.ServerAddress, error)
		//fmt.Println("***Get Chunk Handle: equal")
		array, err := m.csm.ChooseServers(gfs.DefaultNumReplicas)
		if err != nil {
			return errors.New("func (m *Master) RPCGetChunkHandle(args gfs.GetChunkHandleArg, reply *gfs.GetChunkHandleReply) error:\nChoose Servers")
		}
		//func (cm *chunkManager) CreateChunk(path gfs.Path, addrs []gfs.ServerAddress) (gfs.ChunkHandle, error)
		handle, err := m.cm.CreateChunk(args.Path, array)
		if err != nil {
			return errors.New("func (m *Master) RPCGetChunkHandle(args gfs.GetChunkHandleArg, reply *gfs.GetChunkHandleReply) error:\nCreate Chunk")
		}
		reply.Handle = handle
		err = m.csm.AddChunk(array, handle)
		if err != nil {
			return errors.New("func (m *Master) RPCGetChunkHandle(args gfs.GetChunkHandleArg, reply *gfs.GetChunkHandleReply) error:\nAdd Chunk")
		}
		return err

		//func (csm *chunkServerManager) AddChunk(addrs []gfs.ServerAddress, handle gfs.ChunkHandle)

	} else if err != nil {

		return errors.New(
			"func (m *Master) RPCGetChunkHandle(args gfs.GetChunkHandleArg, reply *gfs.GetChunkHandleReply) error:\nGetChunk error")
	}
	reply.Handle = handle
	return nil
}

/*type ListArg struct {
	Path Path
}
type ListReply struct {
	Files []PathInfo
}*/
/*type PathInfo struct {
	Name string

	// if it is a directory
	IsDir bool

	// if it is a file
	Length int64
	Chunks int64
}*/
//func (nm *namespaceManager) GetList(p gfs.Path) ([]gfs.PathInfo, error)
func(m *Master) RPCList(args gfs.ListArg, reply *gfs.ListReply) error {
	m.RLock()
	defer m.RUnlock()
	array, err := m.nm.GetList(args.Path)
	//fmt.Println("len = ", len(array))
	if err != nil {
		errors.New(
			"func(m *Master) RPCList(args gfs.ListArg, reply *gfs.ListReply) error:\nGetList error")
	}
	reply.Files = make([]gfs.PathInfo, 0, 1)
	for i := 0; i < len(array); i++ {
		reply.Files = append(reply.Files, array[i])
		//fmt.Println("arry[i] = ", string(array[i].Name))
	}
	return nil
}