package master

import (
	"gfs"
	"gfs/util"
	"time"
	"sync"
	"errors"
	"os"
	"strconv"
	"strings"
	"io/ioutil"
	//"fmt"
)

// chunkManager manges chunks
type chunkManager struct {
	sync.RWMutex

	chunk map[gfs.ChunkHandle]*chunkInfo
	file  map[gfs.Path]*fileInfo

	numChunkHandle gfs.ChunkHandle
}

type chunkInfo struct {
	sync.RWMutex
	/*
	type RWMutex
    func (rw *RWMutex) Lock()
    func (rw *RWMutex) RLock()
    func (rw *RWMutex) RLocker() Locker
    func (rw *RWMutex) RUnlock()
    func (rw *RWMutex) Unlock()
	*/
	location util.ArraySet     // set of replica locations
	primary  gfs.ServerAddress // primary chunkserver
	expire   time.Time         // lease expire time
	path     gfs.Path
	version  gfs.ChunkVersion
}

type fileInfo struct {
	//sync.RWMutex
	handles []gfs.ChunkHandle
}

type lease struct {
	primary     gfs.ServerAddress
	expire      time.Time
	secondaries []gfs.ServerAddress
}

func (cm *chunkManager) Preserve(serverRoot string) error {
	cm.RLock()
	defer cm.RUnlock()
	//var fileName string = serverRoot + "/" + string(address) + "/" + "cm" + "/" + "chunk"
	//file, err := os.OpenFile(fileName, os.O_CREATE|os.O_RDWR, 0755)
	/*if err != nil {
		return errors.New("func (cm *chunkManager) Preserve:\n Openfile")
	}
	//file.WriteString(strconv.Itoa(len(cm.chunk)) + ",")

	for chunkhandle, info := range cm.chunk {
		array := info.location.GetAll()
		file.WriteString(strconv.Itoa(int(int64(chunkhandle))) + ",")
		file.WriteString(strconv.Itoa(len(array)) + ",")
		for i := 0; i < len(array); i++ {
			file.WriteString(string(array[i].(gfs.ServerAddress)) + ",")
		}
		file.WriteString(strconv.Itoa(int(int64(info.version))) + ",")
		file.WriteString(string(info.path) + ",")
	}
	file.Close()*/
	fileName := serverRoot + "/" + "cm"
	file, err := os.OpenFile(fileName, os.O_CREATE|os.O_RDWR, 0755)
	if err != nil {
		return errors.New("func (cm *chunkManager) Preserve:\n Openfile")
	}
	//file.WriteString(strconv.Itoa(len(cm.file)) + ",")
	for path, info := range cm.file {
		file.WriteString(string(path) + ",")
		file.WriteString(strconv.Itoa(len(info.handles)) + ",")
		for i := 0; i < len(info.handles); i++ {
			file.WriteString(strconv.Itoa(int(int64(info.handles[i]))) + ",")
		}
	}
	file.Close()
	return nil
}
func newChunkManager(serverRoot string, address gfs.ServerAddress) (*chunkManager, error) {
	cm := &chunkManager{
		chunk: make(map[gfs.ChunkHandle]*chunkInfo),
		file:  make(map[gfs.Path]*fileInfo),
	}
	var fileName string = serverRoot + "/" + "cm"// + "/" + "file"
	_, err := os.Stat(fileName)
	exist := false
	if err == nil || os.IsExist(err) {
		exist = true
	}
	if exist {
		file, err1 := os.OpenFile(fileName, os.O_CREATE|os.O_RDONLY, 0755)
		if err1 != nil {
			return cm,errors.New("func (cm *chunkManager) newChunkManager:\n Openfile")
		}

		buf, err2 := ioutil.ReadFile(fileName)
        if err2 != nil {
        	return cm,errors.New("func (cm *chunkManager) newChunkManager:\n Openfile")
        }
       	handle := -1
        length := -1
        cur := 0
        path := ""
        for _, word := range strings.Split(string(buf[0:]), ",") {
        	if path == "" {
        		path = word
        		cm.file[gfs.Path(path)] = new(fileInfo)
        	} else if length == -1 {
        		length, _ = strconv.Atoi(word)
        		cur = 0
        	} else if cur < length {
        		cur++
        		handle, _ = strconv.Atoi(word)
        		cm.file[gfs.Path(path)].handles = append(cm.file[gfs.Path(path)].handles[0:], gfs.ChunkHandle(int64(handle)))
        		if cur == length {
        			handle = -1
        			length = -1
        			cur = 0
        			path = ""
        		}
        	}
        }
        file.Close()
    }
	return cm, nil
}


func (cm *chunkManager) AddChunks(handles []gfs.ChunkHandle, versions []gfs.ChunkVersion, addr gfs.ServerAddress) ([]gfs.ChunkHandle, error) {
	cm.Lock()
	defer cm.Unlock()
	//fmt.Println(".....................AddChunks.............")
	//fmt.Println("Server = ", string(addr))
	ret := make([]gfs.ChunkHandle, 0, 1)
	var err error = nil
	for i := 0; i < len(handles); i++ {
		handle := handles[i]
		_, ok := cm.chunk[handle]
		/*if ok {//???ok
			cm.chunk[handle].Lock()
			if true {//int64(versions[i]) == int64(cm.chunk[handle].version) 
				fmt.Print(int64(handles[i]),",")
				ret = append(ret, handle)
				cm.chunk[handle].location.Add(addr)
				//??? Lease Secondary?
			} else {
				//fmt.Print("*",int64(handles[i]),",")
				//fmt.Print("version given & mine", int64(versions[i]), int64(cm.chunk[handle].version) )
				//ret = append(ret, handle)
				//cm.chunk[handle].location.Add(addr)
			}
			cm.chunk[handle].Unlock() 
		} else {
			cm.chunk[handle] = new
			err = errors.New(
				"func (cm *chunkManager) AddChunks(handles []gfs.ChunkHandle, versions []gfs.ChunkVersion, addr gfs.ServerAddress) ([]gfs.ChunkHandle, error):\nChunkHandle doesn't exist")
		}*/
		if !ok {
			cm.chunk[handle] = new(chunkInfo)
		}
		cm.chunk[handle].Lock()
		ret = append(ret, handle)
		cm.chunk[handle].location.Add(addr)
		cm.chunk[handle].Unlock() 
	}
	//fmt.Println("\n.....................AddEnd.............")
	return ret, err
}
func (cm *chunkManager) DeleteChunk(handle gfs.ChunkHandle, addr gfs.ServerAddress) error {//checked
	cm.Lock()
	defer cm.Unlock()
	cm.chunk[handle].Lock()
	defer cm.chunk[handle].Unlock()
	cm.chunk[handle].location.Delete(addr)
	if cm.chunk[handle].primary == addr {
		cm.chunk[handle].primary = ""
	}

	//fmt.Println("#################################DeleteChunk: handle = ", int64(handle),"addr = ", string(addr))
	var cnt = cm.chunk[handle].location.Size()
	//array := cm.chunk[handle].location.GetAll()
	//for i := 0; i < len(array); i++ {
	//	fmt.Print(string(array[i].(gfs.ServerAddress)), " ")
	//}
	//fmt.Println()
	if cnt < gfs.MinimumNumReplicas {
		return errors.New(
			"func (cm *chunkManager) DeleteChunk(handle gfs.ChunkHandle, addr gfs.ServerAddress) error:\nless than MinimumNumReplicas")
	}
	return nil
}
// RegisterReplica adds a replica for a chunk
func (cm *chunkManager) RegisterReplica(handle gfs.ChunkHandle, from gfs.ServerAddress, to gfs.ServerAddress) error {
	//func (cs *ChunkServer) RPCSendCopy(args gfs.SendCopyArg, reply *gfs.SendCopyReply) error 
	//func (cs *ChunkServer) RPCApplyCopy(args gfs.ApplyCopyArg, reply *gfs.ApplyCopyReply) error
	//func Call(srv gfs.ServerAddress, rpcname string, args interface{}, reply interface{})
	/*type SendCopyArg struct {
	Handle  ChunkHandle
	Address ServerAddress
	}
	type SendCopyReply struct {
	ErrorCode ErrorCode
	}

	type ApplyCopyArg struct {
	Handle  ChunkHandle
	Data    []byte
	Version ChunkVersion
	}
	type ApplyCopyReply struct {
	ErrorCode ErrorCode
	}*/
	//cm.RLock()
	//replicas, _ := cm.GetReplicas(handle)
	//cm.RUnlock()
	cm.Lock()
	defer cm.Unlock()
	cm.chunk[handle].Lock()
	defer cm.chunk[handle].Unlock()
	/*replicasArray := replicas.GetAll()//grammar
	for i := 0; i < len(replicasArray); i++ {
		if replicasArray[i] == addr {
			return errors.New(
				"func (cm *chunkManager) RegisterReplica(handle gfs.ChunkHandle, addr gfs.ServerAddress) error:\nThe ChunkHandle exists in the ServerAddress")
		}
	} 
	if len(replicasArray) == 0 {
		return errors.New(
				"func (cm *chunkManager) RegisterReplica(handle gfs.ChunkHandle, addr gfs.ServerAddress) error:\nThe ChunkHandle doesn't exist")
	}*/
	/*
	var a interface{} = int(10)
	var b MyInt = a.(MyInt)
	*/
	//var sa interface{} = replicas.RandomPick() 
	//var origin gfs.ServerAddress = sa.(gfs.ServerAddress)
	var args1 gfs.SendCopyArg
	var reply1 gfs.SendCopyReply
	args1.Handle = handle
	args1.Address = to
	//fmt.Println("Send&Copy: from = ", string(from),"to = ", string(to), "handle = ", int64(handle))
	util.Call(from, "ChunkServer.RPCSendCopy", args1, &reply1)
	if(reply1.ErrorCode == gfs.Success) {
		cm.chunk[handle].location.Add(to)
		return nil
	} else {
		return errors.New(
			"func (cm *chunkManager) RegisterReplica(handle gfs.ChunkHandle, addr gfs.ServerAddress) error:\nUnknow error")
	}
	//TODO
	//var args2 ApplyCopyArg, reply2 ApplyCopyReply
	//args2.Handle = handle
	//arg2.Data = 
}

// GetReplicas returns the replicas of a chunk
func (cm *chunkManager) GetReplicas(handle gfs.ChunkHandle) (*util.ArraySet, error) {//checked
	cm.RLock()
	defer cm.RUnlock()
	//fmt.Println("^^^^^^^^^^^^^^^^^^  GetReplicas  ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^")
	ret := new(util.ArraySet)
	_, exist := cm.chunk[handle]
	if !exist {
		return ret, errors.New("func (cm *chunkManager) GetReplicas(handle gfs.ChunkHandle) (*util.ArraySet, error):\n The handle doesn't exist")
	}
	cm.chunk[handle].RLock()
	defer cm.chunk[handle].RUnlock()
	//fmt.Println("GetReplicas: handle = ", int64(handle))
	
	tmp, ok := cm.chunk[handle]
	if ok {
		array := tmp.location.GetAll()
		for i := 0; i < len(array); i++ {
			ret.Add(array[i])
			//fmt.Print(string(array[i].(gfs.ServerAddress)), " ")
		}
	}
	//fmt.Println()
	//fmt.Println("\n^^^^^^^^^^^^^^^^^^  GetReplicas  end ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^")
	if ok {
		return ret, nil
	} else {
		return ret, errors.New("func (cm *chunkManager) GetReplicas(handle gfs.ChunkHandle) (*util.ArraySet, error):\n The handle doesn't exist")
	}
}

// GetChunk returns the chunk handle for (path, index).
/*type CreateChunkArg struct {
	Handle ChunkHandle
}
type CreateChunkReply struct {
	ErrorCode ErrorCode
}*/
//func (cm *chunkManager) CreateChunk(path gfs.Path, addrs []gfs.ServerAddress) (gfs.ChunkHandle, error)
func (cm *chunkManager) GetChunk(path gfs.Path, index gfs.ChunkIndex) (gfs.ChunkHandle, error) {//checked
	//fmt.Println("***ok :len & index = ", int(index))
	cm.RLock()
	defer cm.RUnlock()
	//cm.file[path].RLock()
	//defer cm.file[path].RUnlock()
	//fmt.Println("***ok :len & index = ", len(cm.file[path].handles), int(index))
	fileInfomation, ok := cm.file[path]
	if !ok {
		return *new(gfs.ChunkHandle), errors.New("func (cm *chunkManager) GetChunk(path gfs.Path, index gfs.ChunkIndex) (gfs.ChunkHandle, error):\n Error")
		cm.file[path] = new(fileInfo)
		cm.file[path].handles = make([]gfs.ChunkHandle, 0, 1)
		fileInfomation, ok = cm.file[path]
	}
	//fmt.Println("@@@ len & index = ", len(cm.file[path].handles), int(index))
	if len(cm.file[path].handles) > int(index) {
		//fmt.Println("@@@ Condition  1\n")
		return fileInfomation.handles[index], nil
	} else if len(fileInfomation.handles) == int(index) {
		//fmt.Println("@@@ Condition  2\n")
		return *(new(gfs.ChunkHandle)), errors.New(
			"func (cm *chunkManager) GetChunk(path gfs.Path, index gfs.ChunkIndex) (gfs.ChunkHandle, error):\nThe index equals to the number of all the chunks plus one")
	} else {
			//fmt.Println("@@@ Condition  2\n")
		return *(new(gfs.ChunkHandle)), errors.New(
			"func (cm *chunkManager) GetChunk(path gfs.Path, index gfs.ChunkIndex) (gfs.ChunkHandle, error):\nThe index is bigger than the number of all the chunks plus one")
	}
	return *new(gfs.ChunkHandle), nil

}

// GetLeaseHolder returns the chunkserver that hold the lease of a chunk
// (i.e. primary) and expire time of the lease. If no one has a lease,
// grants one to a replica it chooses.
func (cm *chunkManager) GetLeaseHolder(handle gfs.ChunkHandle) (*lease, error) {//checked
	
	cm.Lock()
	defer cm.Unlock()
	cm.chunk[handle].Lock()
	defer cm.chunk[handle].Unlock()
	//fmt.Println("^^^^^^^^^^^^^^^^^^GetLeaseHolder^^^^^^^^^^^^^^^^^^^^^^^^")
	tmp, ok := cm.chunk[handle]
	if !ok {
		return nil, errors.New(
			"func (cm *chunkManager) GetLeaseHolder(handle gfs.ChunkHandle) (*lease, error):\nThe handle doesn't exist")
	}
	ret := &lease{}
	if tmp.primary == "" {
		var sa interface{} = tmp.location.RandomPick()
		cm.chunk[handle].primary = sa.(gfs.ServerAddress)
	}

	//now := time.Now();
	//now = now.Add(time.Minute)
	//cm.chunk[handle].expire = now

	tmp, ok = cm.chunk[handle]
	ret.primary = tmp.primary
	//fmt.Println("primary = ", string(ret.primary))
	array := tmp.location.GetAll()
	ret.secondaries = make([]gfs.ServerAddress, 0, 1)
	for i := 0; i < len(array); i++ {
		var elem = array[i].(gfs.ServerAddress)
		if elem != tmp.primary {
			ret.secondaries = append(ret.secondaries[0:], elem)
			//fmt.Println("secondaries = ", string(ret.secondaries[len(ret.secondaries) - 1]))
		}
	}
	ret.expire = cm.chunk[handle].expire
	cm.chunk[handle].version = gfs.ChunkVersion(int(cm.chunk[handle].version) + 1)
	//fmt.Println("^^^^^^^^^^^^^^^^^^End^^^^^^^^^^^^^^^^^^^^^^^^")
	//fmt.Println("Primary = ", ret.primary, "Secondaries = ", ret.secondaries)
	return ret, nil
}

// ExtendLease extends the lease of chunk if the lease holder is primary.
func (cm *chunkManager) ExtendLease(handle gfs.ChunkHandle, primary gfs.ServerAddress) error {//checked
	cm.Lock()
	defer cm.Unlock()
	cm.chunk[handle].Lock()
	defer cm.chunk[handle].Unlock()
	if cm.chunk[handle].primary != primary {
		return errors.New(
			"func (cm *chunkManager) ExtendLease(handle gfs.ChunkHandle, primary gfs.ServerAddress) error:\nThe lease holder is not primary")
	}
	now := time.Now();
	now = now.Add(gfs.LeaseExpire )
	cm.chunk[handle].expire = now
	return nil
}

// CreateChunk creates a new chunk for path.
func (cm *chunkManager) CreateChunk(path gfs.Path, addrs []gfs.ServerAddress) (gfs.ChunkHandle, error) { //checked
	
/*type chunkManager struct {
	sync.RWMutex

	chunk map[gfs.ChunkHandle]*chunkInfo
	file  map[gfs.Path]*fileInfo

	numChunkHandle gfs.ChunkHandle
}

type chunkInfo struct {
	sync.RWMutex
	location util.ArraySet     // set of replica locations
	primary  gfs.ServerAddress // primary chunkserver
	expire   time.Time         // lease expire time
	path     gfs.Path
}

type fileInfo struct {
	handles []gfs.ChunkHandle
}*/
/*type CreateChunkArg struct {
	Handle ChunkHandle
}
type CreateChunkReply struct {
	ErrorCode ErrorCode
}*/
//func CallAll(dst []gfs.ServerAddress, rpcname string, args interface{}) error {

	cm.Lock()
	defer cm.Unlock()
	//cm.file[path].Lock())
	//defer cm.file[path].Unlock()
	id := gfs.ChunkHandle(len(cm.chunk))
	cm.chunk[id] = new(chunkInfo)
	//fmt.Println("id = ", len(cm.chunk))
	cm.chunk[id].Lock()
	defer cm.chunk[id].Unlock()
	for i := 0; i < len(addrs); i++ {
		cm.chunk[id].location.Add(addrs[i])
	}
	cm.chunk[id].path = path
	cm.chunk[id].primary = ""
	_, ok := cm.file[path]
	if !ok {
		cm.file[path] = new(fileInfo)
		cm.file[path].handles = make([]gfs.ChunkHandle, 0, 1)
	}
	cm.file[path].handles = append(cm.file[path].handles, id)
	//TODO func (cs *ChunkServer) RPCCreateChunk(args gfs.CreateChunkArg, reply *gfs.CreateChunkReply) error
	var args1 gfs.CreateChunkArg
	args1.Handle = id
	//fmt.Println("***********************************Create*******************************************")
	//for i := 0; i < len(addrs); i++ {
	//	fmt.Println("***addr & handle", string(addrs[i]), int64 (args1.Handle))
	//}
	//fmt.Println("***********************************************************************************")
	//fmt.Println("handle = ", int64(id))
	//if int64(id) == int64(40) {
		//for i := 0; i < len(addrs); i++{
		//	fmt.Println("Address = ", addrs[i])
		//}

	err := util.CallAll(addrs, "ChunkServer.RPCCreateChunk", args1)
	//if err != nil {
		//fmt.Println("###############################ERROR\n")
	//}
	//fmt.Println("\n***********************************************************************************")
	
	//if err != nil {
	//	fmt.Println("***CALL ALL ERROR")
	//}
	if err == nil {
		return id, nil
	} else {
	//	fmt.Println("***what")
		return id, errors.New(
			"func (cm *chunkManager) CreateChunk(path gfs.Path, addrs []gfs.ServerAddress) (gfs.ChunkHandle, error):\nCallAll error")
	}
	
}
