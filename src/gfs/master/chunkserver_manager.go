package master

import (
	"gfs"
	//"gfs/util"
	"sync"
	"time"
	//"os"
	//"strings"
	//"strconv"
	"errors"
	//"fmt"
)

// chunkServerManager manages chunkservers
type chunkServerManager struct {
	sync.RWMutex
	servers map[gfs.ServerAddress]*chunkServerInfo
}
type chunkServerInfo struct {
	lastHeartbeat time.Time
	chunks        map[gfs.ChunkHandle]bool // set of chunks that the chunkserver has
}

func newChunkServerManager(serverRoot string, address gfs.ServerAddress) *chunkServerManager {
	csm := &chunkServerManager{
		servers: make(map[gfs.ServerAddress]*chunkServerInfo),
	}
	/*var fileName string = serverRoot + "/" + string(address) + "/" + "csm"
	_, err := os.Stat(fileName)
	exist := false
	if err == nil || os.IsExist(err) {
		exist = true
	}
	if exist {
		file, err1 := os.OpenFile(fileName, os.O_CREATE|os.O_RDWR, 0755)
		defer file.Close()
		if err1 != nil {
			return csm
		}
		//strings.Split(string(buf), ",")
		buf := make([]byte, 1000000)
        size, err2 := file.Read(buf)
        if err2 != nil {
			return csm
		}
        mapSize := 0
        cur := -1
        address := ""
        for _, word := range strings.Split(string(buf[0 : size]), ",") {
        	if address == "" {
        		address = word
        		cur = -1
        		csm.servers[gfs.ServerAddress(address)] = new(chunkServerInfo)
        		csm.servers[gfs.ServerAddress(address)].chunks = make(map[gfs.ChunkHandle]bool, 0)
        	} else if address != "" && cur == -1 {
        		cur = 0
        		mapSize, _ = strconv.Atoi(word)
        	} else if cur < mapSize {
        		handle, _ := strconv.Atoi(word)
        		csm.servers[gfs.ServerAddress(address)].chunks[gfs.ChunkHandle(int64(handle))] = true
        		cur++
        	} else if cur == mapSize {
        		//time := word//???
        		mapSize = 0
        		cur = -1
        		address = ""
        	}
        }
	}*/
	return csm
}



/*func (csm *chunkServerManager) Preserve(serverRoot string, address gfs.ServerAddress) error{
	csm.RLock()
	defer csm.RUnlock()
	var fileName string = serverRoot + "/" + string(address) + "/" + "csm"
	file, err := os.OpenFile(fileName, os.O_CREATE|os.O_RDWR, 0755)
	if err != nil {
		return errors.New("func (csm *chunkServerManager) Preserve:\nOpenfile")
	}
	defer file.Close()
	for address, info := range csm.servers {
		size := 0
		file.WriteString(string(address) + ",")
		for _, exist := range info.chunks {
				//file.WriteString(strconv.Itoa(len(info.chunks)) + ",")
			if exist {
				size++
			}
		}
		file.WriteString(strconv.Itoa(size) + ",")
		for chunkhandle, exist := range info.chunks {
			if exist {
				file.WriteString(strconv.Itoa(int(chunkhandle)) + ",")
			}
		}
		//file.WriteString(string(info.lastHeartbeat) + ",")//???
		file.WriteString("*" + ",")
			
	}
	return nil
}*/
// Hearbeat marks the chunkserver alive for now.
func (csm *chunkServerManager) Heartbeat(addr gfs.ServerAddress) bool {//checked
	//grammar
	//fmt.Println("$$$$$$$$$$$ Heartbeat $$$$$$$$$$$$$$$$", string(addr))
	csm.Lock()
	defer csm.Unlock()
	_, ok := csm.servers[addr]
	//fmt.Println("hello ok = ", ok)
	if !ok {
		//csm.servers[addr].chunks = make(map[gfs.ChunkHandle]bool)
		csm.servers[addr] = new(chunkServerInfo)
		csm.servers[addr].chunks = make(map[gfs.ChunkHandle]bool)
		csm.servers[addr].lastHeartbeat = time.Now()
		return false
	}
	csm.servers[addr].lastHeartbeat = time.Now()
	return true
}

// AddChunk creates a chunk on given chunkservers
func (csm *chunkServerManager) UpdateChunkServer(addr gfs.ServerAddress, handles []gfs.ChunkHandle) error {
	//fmt.Println("$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$", string(addr))
	csm.Lock()
	defer csm.Unlock()
	/*_, ok := csm.servers[addr]
	if !ok {
		return errors.New(
			"func (csm *chunkServerManager) UpdateChunkServer(addr gfs.ServerAddress, handles []gfs.ChunkHandle) error:\nChunkServer doesn't exist")
	}*/
	csm.servers[addr].chunks = make(map[gfs.ChunkHandle]bool, 0)
	//fmt.Println("=================start==============")
	//fmt.Println("address = ",string(addr))
	//fmt.Print("handles : ")
	for i := 0; i < len(handles); i++ {
		//fmt.Print(int64(handles[i])," ")
		csm.servers[addr].chunks[handles[i]] = true
	}
	//fmt.Println("\n=================end==============")
	return nil
}
/*func (csm *chunkServerManager) DeleteOldVersionChunk(addr gfs.ServerAddress, handle gfs.ChunkHandle) error {
	csm.Lock()
	defer csm.Unlock()
	chunkServer, ok := csm.servers[addr]
	if !ok {
		return errors.New(
			"func (csm *chunkServerManager) DeleteOldVersionChunk(addr gfs.ServerAddress, handle gfs.ChunkHandle) error:\nChunkServer doesn't exist")
	}
	handleExist, _ := chunkServer.chunks[handle]
	if handleExist == false {
		return errors.New(
			"func (csm *chunkServerManager) DeleteOldVersionChunk(addr gfs.ServerAddress, handle gfs.ChunkHandle) error:\nChunkHandle doesn't exist")
	}
	chunkServer.chunks[handle] = false
	return nil
}*/

func (csm *chunkServerManager) AddChunk(addrs []gfs.ServerAddress, handle gfs.ChunkHandle) error {//checked
	//func (cs *ChunkServer) RPCCreateChunk(args gfs.CreateChunkArg, reply *gfs.CreateChunkReply) error
/*type CreateChunkArg struct {
	Handle ChunkHandle
}
type CreateChunkReply struct {
	ErrorCode ErrorCode
}*/
//fmt.Println("CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCccc")
//for i := 0; i < len(addrs); i++{
//	fmt.Print(addrs[i],",")

//}

	csm.Lock()
	defer csm.Unlock()
	//var arg gfs.CreateChunkArg
	//arg.Handle = handle
	for i := 0; i < len(addrs); i++ {
		tmp, _ := csm.servers[addrs[i]].chunks[handle]
		if tmp {
			return errors.New(
			"func (csm *chunkServerManager) AddChunk(addrs []gfs.ServerAddress, handle gfs.ChunkHandle) error:\nChunk exist error")
		}
		csm.servers[addrs[i]].chunks[handle] = true
		//fmt.Println("map:", addrs[i], csm.servers[addrs[i]].chunks[handle])
	}
//fmt.Println("\nCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCccc")
	//if err := util.CallAll(addrs, "ChunkServer.RPCCreateChunk", arg); err == nil {
	//	return nil
	//} else {
	//	return errors.New(
	//		"func (csm *chunkServerManager) AddChunk(addrs []gfs.ServerAddress, handle gfs.ChunkHandle) error:\nCallAll error")
	//}
	return nil
}

// ChooseReReplication chooses servers to perform re-replication
// called when the replicas number of a chunk is less than gfs.MinimumNumReplicas
// returns two server address, the master will call 'from' to send a copy to 'to'
func (csm *chunkServerManager) ChooseReReplication(handle gfs.ChunkHandle) (from, to gfs.ServerAddress, err error) {
	csm.RLock()
	defer csm.RUnlock()
	//fmt.Println("Choose : handle:",int64(handle))
	to = ""
	flag := false
	minn := -1
	cnt := 0
	for name, obj := range csm.servers {
		//fmt.Println("Choose:names:",string(name))
		if _, ok := obj.chunks[handle]; !ok {
			if !flag {
				minn = len(obj.chunks)
				flag = true
				to = name
			} else if len(obj.chunks) < minn {
				minn = len(obj.chunks)
				to = name
			} 
		} else {
			from = name
			cnt ++
		} 	
	} 
	//fmt.Println("return : cnt = ", cnt, "from = ", string(from), "to = ", string(to))
	if cnt >= 3 { 
		return from, to, errors.New("func (csm *chunkServerManager) ChooseReReplication(handle gfs.ChunkHandle) (from, to gfs.ServerAddress, err error):\nNo need to ReReplication")
	} else if cnt == 0 {
		return from, to, errors.New("func (csm *chunkServerManager) ChooseReReplication(handle gfs.ChunkHandle) (from, to gfs.ServerAddress, err error):\nThe handle doesn't exist")
	} else if to == "" {
		return from, to, errors.New("func (csm *chunkServerManager) ChooseReReplication(handle gfs.ChunkHandle) (from, to gfs.ServerAddress, err error):\nThe ChunkServer who doesn't have this handle doesn't exist")
	} else {
		//fmt.Println("ChooseReReplication : chunkhandle & from & to:", int64(handle), string(from), string(to))
		return from, to, nil
	}

}

// ChooseServers returns servers to store new chunk.
// It is called when a new chunk is create
func (csm *chunkServerManager) ChooseServers(num int) ([]gfs.ServerAddress, error) {//checked
	
	csm.RLock()
	defer csm.RUnlock()
	ret := make([]gfs.ServerAddress, 0, 1)
	for i := 0; i < num; i++ {
		flag := false
		var minn int
		for name, obj := range csm.servers {
			ok := true
			for j := 0; j < len(ret); j++ {
				if name == ret[j] {
					ok = false
					break
				}
			}
			if ok && !flag {
				minn = len(obj.chunks)
				flag = true
				ret = append(ret[0:], name)
			} else if ok && flag && len(obj.chunks) < minn{
				minn = len(obj.chunks)
				ret[i] = name
			} 
		}
	}
	//for name, _ := range csm.servers {
	//	 ret = append(ret[0:], name)
	//	 if len(ret) == num {
	//	 	break
	//	 }
	//}
	//fmt.Print("handle = ", int64(handle), "ret = ")
	/*for i := 0; i < num; i++ {
		fmt.Print(string(ret[i]), " ")
	}*/
	if len(ret) < num {
		return ret, errors.New("func (csm *chunkServerManager) ChooseServers(num int) ([]gfs.ServerAddress, error):\nThe num of chunkservers is not enough")
	}
	return ret, nil
}



// DetectDeadServers detects disconnected chunkservers according to last heartbeat time
func (csm *chunkServerManager) DetectDeadServers() []gfs.ServerAddress {//checked
	csm.RLock()
	defer csm.RUnlock()
	//fmt.Println("WWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWW")
	now := time.Now()
	ret := make([]gfs.ServerAddress, 0, 1)
	for name, obj := range csm.servers {
		t := obj.lastHeartbeat.Add(gfs.ServerTimeout)
		//fmt.Println("name = ",name, "lastTime = ", obj.lastHeartbeat, "TimeNow = ", now, "wait = ", gfs.ServerTimeout)
		if now.After(t) {
			ret = append(ret[0:], name)
		}
	}
	//fmt.Println("WWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWW")
	return ret
}

// RemoveServers removes metedata of a disconnected chunkserver.
// It returns the chunks that server holds
func (csm *chunkServerManager) RemoveServer(addr gfs.ServerAddress) (handles []gfs.ChunkHandle, err error) {//checked
	csm.Lock()
	defer csm.Unlock()
	obj, ok := csm.servers[addr]
	handles = make([]gfs.ChunkHandle, 0, 1)
	if !ok {
		err = errors.New("func (csm *chunkServerManager) RemoveServer(addr gfs.ServerAddress) (handles []gfs.ChunkHandle, err error):\nThe ChunkServer doesn't exist")
		return
	}
	for name, _ := range obj.chunks {
		handles = append(handles, name)
		//delete(obj.chunks, name)
	}
	delete(csm.servers, addr)
	err = nil
	//fmt.Println("_______________________________REMOVESERVERS______________________")
	//fmt.Println("address : ", string(addr))
	//for i := 0; i < len(handles); i++ {
	//	fmt.Print(int64(handles[i]), " ")
	//}
	//fmt.Println("\n__________________________________________")
	return
	//delete(servers, addr)
	//return ret, nil
}
