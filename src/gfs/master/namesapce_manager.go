package master

import (
	"gfs"
	"errors"
	"sync"
	"os"
	"strings"
	"strconv"
	"io/ioutil"
	//"fmt"
)

type namespaceManager struct {
	sync.RWMutex
	root *nsTree
	count int
}

type nsTree struct {
	/*
	type RWMutex
    func (rw *RWMutex) Lock()
    func (rw *RWMutex) RLock()
    func (rw *RWMutex) RLocker() Locker
    func (rw *RWMutex) RUnlock()
    func (rw *RWMutex) Unlock()
	*/
	sync.RWMutex

	// if it is a directory
	isDir    bool
	children map[string]*nsTree

	// if it is a file
	chunks int64
	path string
}
func Exist(filename string) bool {
    _, err := os.Stat(filename)
    return err == nil || os.IsExist(err)
}
func newNamespaceManager(serverRoot string) (*namespaceManager, error) {
	nm := &namespaceManager{
		root: &nsTree {
			isDir: true,
			children: make(map[string]*nsTree),
		},
	}
	var fileName string = serverRoot + "/" + "nm"
	_, err := os.Stat(fileName)
	exist := false
	if err == nil || os.IsExist(err) {
		exist = true
	}
	if exist {
		file, err1 := os.OpenFile(fileName, os.O_CREATE|os.O_RDONLY, 0755)
		defer file.Close()
		if err1 != nil {
			return nm,errors.New("func newNamespaceManager:\nOpenFile")
		}
		//strings.Split(string(buf), ",")
		buf, err2 := ioutil.ReadFile(fileName)
        if err2 != nil {
			return nm,errors.New("func newNamespaceManager:\nRead")
		}
        isdir := true
       	path := ""
       	num := 0
        for index, word := range strings.Split(string(buf[0:]), ",") {
        	if index % 3 == 0 {
        		if word == "true"{
        			isdir = true
        		} else {
        			isdir = false
        		}
        	} else if index % 3 == 1 {
        		path = word
        	} else {
        		num, _ =  strconv.Atoi(word)
        		if isdir {
        			err3 := nm.Mkdir(gfs.Path(path), num)
        			if err3 != nil {
						return nm,errors.New("func newNamespaceManager:\nMkdir")
					}
        			//if err3 != nil {}
        		} else {
        			err4 := nm.Create(gfs.Path(path), num)
        			if err4 != nil {
						return nm,errors.New("func newNamespaceManager:\nCreate")
					}
        			//if err4 != nil {}
        		}
        	}
        }
        //if err2 != nil && err2 != io.EOF {}
	}
	return nm, nil
}

func newnsTree(isDir bool, chunks int, path string) *nsTree{
	nt := &nsTree{
		isDir: isDir,
		children: make(map[string]*nsTree),
		chunks: int64(chunks),
		path: path,
	}
	return nt
}

func (nm *namespaceManager) Preserve(serverRoot string) error{
	nm.Lock()
	defer nm.Unlock()
	queue := make([]*nsTree, 0, 1)
	queue = append(queue, nm.root)
	//queue_name := make([]string, 0, 1)
	//queue_name := append(queue_name, "root")
	var fileName string = serverRoot + "/" + "nm"
	file, err := os.OpenFile(fileName, os.O_CREATE|os.O_RDWR, 0755)
	defer file.Close()
	if err != nil {
		return errors.New("func (nm *namespaceManager) Preserve:\nOpenFile")
	}
	//file.WriteString("Just a test!\r\n")
	//file.WriteString(strconv.Itoa(nm.count) + ",")
	start := 0
	end := 1
	for start < end {
		queue[start].RLock()
		for _, obj := range queue[start].children {
			obj.RLock()
			tmp := ""
			if obj.isDir {
				tmp = "true"
			} else {
				tmp = "false"
			}
			file.WriteString(tmp + "," + obj.path + "," + strconv.Itoa(int(obj.chunks)) + ",")
			//fmt.Printf(obj.path, obj.isDir, obj.chunks)
			queue = append(queue, obj)
			end ++
			obj.RUnlock()
		}
		queue[start].RUnlock()
		start ++
	}
	return nil
}
// Create creates an empty file on path p. All parents should exist.
func (nm *namespaceManager) Create(p gfs.Path, num int) error {//checked
	tmp := ""
	now := nm.root
	for i, pi := range  p {
		if i == 0{
			continue
		}
		if pi != rune('/') {
			tmp += string(pi)
		} else {
			if nex, ok := now.children[tmp]; ok {
				nex.Lock()
				now = nex;
				defer now.Unlock()
			} else {
				return errors.New(
					"func (nm *namespaceManager) Create(p gfs.Path) error:\n The path doesn't exist")
			}
			tmp = "";
		}
	}
	//fmt.Println("***now.Name = ", string(tmp))
	_, ok := now.children[tmp]
	if ok {
		return errors.New("func (nm *namespaceManager) Create(p gfs.Path) error:\n Create twice")
	}
	now.children[tmp] = newnsTree(false, num, string(p))
	//Don't create the chunks
	nm.count++
	return nil
}

// Mkdir creates a directory on path p. All parents should exist.
func (nm *namespaceManager) Mkdir(p gfs.Path, num int) error {//checked
	//fmt.Println("______________Mkdir____________path = ", string(p))
	tmp := ""
	now := nm.root
	for i, pi := range  p {
		if i == 0{
			continue
		}
		if pi != rune('/') {
			tmp += string(pi)

		} else {
			if nex, ok := now.children[tmp]; ok {
				nex.Lock()
				now = nex;
				defer now.Unlock()
			} else {
				return errors.New(
					"func (nm *namespaceManager) Mkdir(p gfs.Path) error:\n The path doesn't exist")
			}
			tmp = "";
		}
	}
	//fmt.Println("tmp = ", string(tmp), "bool = ", now == nm.root)
	_, ok := now.children[tmp]
	if ok {
		return errors.New("func (nm *namespaceManager) Mkdir(p gfs.Path) error:\n Mkdir twice")
	}
	now.children[tmp] = newnsTree(true, num, string(p))
	_, ok = now.children[tmp]
	//fmt.Println("tmp = ", string(tmp), "ok = ", ok)
	nm.count++
	return nil
}

/*type PathInfo struct {
	Name string

	// if it is a directory
	IsDir bool

	// if it is a file
	Length int64
	Chunks int64
}*/
func (nm *namespaceManager) GetList(p gfs.Path) ([]gfs.PathInfo, error) {
	tmp := ""
	now := nm.root
	for i, pi := range p {
		if i == 0{
			continue
		}
		if pi != rune('/') {
			tmp += string(pi)
		}
		if pi == rune('/') || i == len(p) - 1 {
			if nex, ok := now.children[tmp]; ok {
				nex.RLock()
				now = nex;
				defer now.RUnlock()
			} else {
				return make([]gfs.PathInfo, 0, 1), errors.New(
					"func (nm *namespaceManager) GetList(p gfs.Path) ([]gfs.PathInfo, error):\n The path doesn't exist")
			}
			if i != len(p) - 1 {
				tmp = "";
			}
		}
	}
	array := make([]gfs.PathInfo, 0, 1)
	for name, obj := range now.children {
		obj.RLock()
		var element gfs.PathInfo
		element.IsDir = obj.isDir
		element.Chunks = obj.chunks
		//element.Length = obj.length
		element.Name = name
		array = append(array[0:], element)
		obj.RUnlock()
	}
	return array, nil 
}
