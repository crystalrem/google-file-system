package client

import (
	"gfs"
	"gfs/util"
	log "github.com/Sirupsen/logrus"
	"sort"
	"errors"
	"io"
	//"fmt"
	"sync"
	//"errors"
)

// Client struct is the GFS client-side driver
type Client struct {
	sync.RWMutex
	master gfs.ServerAddress
	//ch2cs  map[gfs.ChunkHandle]*gfs.ServerAddress
}

// NewClient returns a new gfs client.
func NewClient(master gfs.ServerAddress) *Client {
	return &Client{
		master: master,
		//ch2cs: make(map[gfs.ChunkHandle]*gfs.ServerAddress),
	}
}

// Create creates a new file on the specific path on GFS.
func (c *Client) Create(path gfs.Path) error {
	arg := gfs.CreateFileArg {
		Path: path,
	}
	rep := new(gfs.CreateFileReply)
	err := util.Call(c.master, "Master.RPCCreateFile", arg, &rep)
	if err != nil {
		log.Fatal("Client Creat File Error:", err)
		log.Exit(1)
	}
	return err;
}

// Mkdir creates a new directory on GFS.
func (c *Client) Mkdir(path gfs.Path) error {
	arg := gfs.MkdirArg {
		Path: path,
	}
	rep := new(gfs.MkdirReply)
	err := util.Call(c.master, "Master.RPCMkdir", arg, &rep)
	if err != nil {
		log.Fatal("Client Make Directory Error:", err)
		log.Exit(1)
	}
	return err;
}

// List lists everything in specific directory on GFS.
func (c *Client) List(path gfs.Path) ([]gfs.PathInfo, error) {
	arg := gfs.ListArg {
		Path: path,
	}
	rep := new(gfs.ListReply)
	err := util.Call(c.master, "Master.RPCList", arg, &rep)
	if err != nil {
		log.Fatal("Client List Error:", err)
		log.Exit(1)
	}
	ret := rep.Files
	return ret, err
}

// Read reads the file at specific offset.
// It reads up to len(data) bytes form the File.
// It return the number of bytes, and an error if any.
func (c *Client) Read(path gfs.Path, offset gfs.Offset, data []byte) (n int, err error) {
	length := len(data)
	n = 0
	//fmt.Println(length, len(data), "clientreadbegin")
	num := offset
	cur := offset
	if int64(offset) % int64(gfs.MaxChunkSize) == 0 {
		cur = offset / gfs.MaxChunkSize
		num = 0
	} else {
		cur = offset / gfs.MaxChunkSize
		num = offset % gfs.MaxChunkSize
	}
	//fmt.Println("***for start ======================================== ")
	for length > 0 {
		curlen := gfs.MaxChunkSize - num
		//fmt.Println("***for cur & curlen = ", cur, curlen)
		if int(curlen) > length {
			curlen = gfs.Offset(length)
		}
		d := make([]byte, curlen)
		curCH, err0 := c.GetChunkHandle(path, gfs.ChunkIndex(cur))
		if err0 != nil {
			log.Fatal("Client Get ChunkHandle Error(Read):", err)
			log.Exit(1)
		}
		//fmt.Println(num, len(d), "clientread")
		l, err := c.ReadChunk(curCH, num, d)
		//fmt.Println("Read!", path, offset)
		length = length - l
		//fmt.Println("***for length & l = ", length, l)
		if err != nil {
			if err.Error() == "EOF" {
				err = io.EOF
			} else {
				//log.Fatal("Client Read Error:", err)
				//fmt.Println("Read!")
				return n + l, err
			}
		}
		//fmt.Println(l, len(d), len(data), "clientreadF")
		for i := 0; i < l; i ++ {
			data[n + i] = d[i]
		}
		n = n + l
		//fmt.Println(string(path), "cur:", cur, data[0:n], n, offset,curCH, "clientreadend")
		if err == io.EOF {
			//fmt.Println(string(path), "cur:", cur, data[0:n], n, offset,curCH, "clientreadend")
			return n, err
		}
		cur ++
		num = 0;
	}
	//fmt.Println(data, n, offset, curCH, "clientreadend")
	//fmt.Println("Read!")
	return n, err
}

// Write writes data to the file at specific offset.
func (c *Client) Write(path gfs.Path, offset gfs.Offset, data []byte) error {
	L := len(data)
	length := L
	num := offset
	cur := offset
	if offset % gfs.MaxChunkSize == 0 {
		cur = offset / gfs.MaxChunkSize
		num = 0
	} else {
		cur = offset / gfs.MaxChunkSize
		num = offset % gfs.MaxChunkSize
	}
	for length > 0 {
		curlen := gfs.MaxChunkSize - num
		if int(curlen) > length {
			curlen = gfs.Offset(length)
		}
		d := data[int(L) - length:int(L) - length + int(curlen)]
		curCH, err := c.GetChunkHandle(path, gfs.ChunkIndex(cur))
		if err != nil {
			log.Fatal("Client Get ChunkHandle Error(Write):", err)
			log.Exit(1)
		}
		err1 := c.WriteChunk(curCH, num, d)
		if err1 != nil {
			//log.Fatal("Client Write Error:", err1)
			return err
		}
		length = length - int(curlen)
		cur ++
		num = 0;
	}
	return nil
}

// Append appends data to the file. Offset of the beginning of appended data is returned.
func (c *Client) Append(path gfs.Path, data []byte) (offset gfs.Offset, err error) {
	L := len(data)
	length := L
	//fmt.Println(string(path), data, offset, "clientappendbegin")
	cur := 0
	f := false
	for length > 0 {
		curlen := length
		if curlen > gfs.MaxAppendSize {
			curlen = gfs.MaxAppendSize
		}
		curCH, err := c.GetChunkHandle(path, gfs.ChunkIndex(cur))
		if err != nil {
			//log.Fatal("Client Get ChunkHandle Error(Append):", err)
			log.Exit(1)
		}
		d := data[L - length:L - length + curlen]
		//fmt.Println("================== START APPEND ==================\n", d)
		off, err, ErrId := c.AppendChunk(curCH, d)
		if err != nil {
			//log.Fatal("Client Append Error:", err)
			return 0, err
		}
		if ErrId == 0 {
			if f == false {
				offset = off
				f = true
			}
			//fmt.Println("!!!!!!!!!!!!!!!!!!!", ErrId, cur, data)
			length = length - curlen
		} else {
			//fmt.Println("???????????????????", ErrId, cur, data)
			cur ++
		}
	}
	offset = gfs.Offset(cur) * gfs.MaxChunkSize + offset
	//fmt.Println(offset)
	return offset, err
}

// GetChunkHandle returns the chunk handle of (path, index).
// If the chunk doesn't exist, master will create one.
func (c *Client) GetChunkHandle(path gfs.Path, index gfs.ChunkIndex) (gfs.ChunkHandle, error) {
	arg := gfs.GetChunkHandleArg {
		Path:  path,
		Index: index,
	}
	rep := new(gfs.GetChunkHandleReply)
	err := util.Call(c.master, "Master.RPCGetChunkHandle", arg, &rep)
	if err != nil {
		log.Fatal("Client Get Chunk Handle Error:", err)
		log.Exit(1)
	}
	ret := rep.Handle
	return ret, err
}

// ReadChunk reads data from the chunk at specific offset.
// len(data)+offset  should be within chunk size.
func (c *Client) ReadChunk(handle gfs.ChunkHandle, offset gfs.Offset, data []byte) (int, error) {
	length := len(data)
	argreplicas := gfs.GetReplicasArg {
		Handle: handle,
	}
	//fmt.Println(len(data), int(offset), "readchunkbegin")
	repreplicas := new(gfs.GetReplicasReply)
	err := util.Call(c.master, "Master.RPCGetReplicas", argreplicas, &repreplicas)
	if err != nil {
		log.Fatal("Client Get Chunkserver(Read) Error:", err);
		log.Exit(1)
	}
	//fmt.Println("QAQAQ0", handle)
	loc := make([]string, len(repreplicas.Locations))
	for i := 0; i < len(repreplicas.Locations); i ++ {
		loc[i] = string(repreplicas.Locations[i])
	}
	sort.Strings(loc)
	for i := 0; i < len(repreplicas.Locations); i ++ {
		repreplicas.Locations[i] = gfs.ServerAddress(loc[i])
	}
	//fmt.Println("QAQAQ1", handle)
	errf := errors.New("")
	rep := new(gfs.ReadChunkReply)
	arg := gfs.ReadChunkArg {}
	for i := 0; i < len(repreplicas.Locations); i ++ {
		CScur := repreplicas.Locations[i]
		rep = new(gfs.ReadChunkReply)
		arg = gfs.ReadChunkArg {
			Handle: handle,
			Offset: offset,
			Length: length,
		}
		err0 := util.Call(CScur, "ChunkServer.RPCReadChunk", arg, &rep)
		if err0 == nil  {
			errf = nil
			break
		} else {
			errf = err0
		}
	}
	n := rep.Length
	//fmt.Println(len(data), len(rep.Data), int(rep.Length), "client")
	for i := 0; i < rep.Length; i ++ {
		data[i] = rep.Data[i]
	}
	if rep.ErrorCode == 4 {
		errf = io.EOF
	}
	//fmt.Println("^^^^^^^^^^^^^", rep.Length)
	//fmt.Println("QAQAQ2")
	return n, errf
}

// WriteChunk writes data to the chunk at specific offset.
// len(data)+offset should be within chunk size.
func (c *Client) WriteChunk(handle gfs.ChunkHandle, offset gfs.Offset, data []byte) error { 
	argreplicas := gfs.GetReplicasArg {
		Handle: handle,
	}
	//fmt.Println(data, int64(offset), 0)
	TryAgainWrite:
	repreplicas := new(gfs.GetReplicasReply)
	err := util.Call(c.master, "Master.RPCGetReplicas", argreplicas, &repreplicas)
	if err != nil {
		log.Fatal("Client Get Chunkserver(Write) Error:", err);
		log.Exit(1)
	}
	loc := make([]string, len(repreplicas.Locations))
	for i := 0; i < len(repreplicas.Locations); i ++ {
		loc[i] = string(repreplicas.Locations[i])
	}
	sort.Strings(loc)
	//fmt.Println("=== ForwardAddress ===================")
	for i := 0; i < len(repreplicas.Locations); i ++ {
		repreplicas.Locations[i] = gfs.ServerAddress(loc[i])
		//fmt.Print(repreplicas.Locations[i], " ")
	}
	//fmt.Println()
	CSnext := repreplicas.Locations[0]
	CSforward := repreplicas.Locations[1:]
	arggetp := gfs.GetPrimaryAndSecondariesArg {
		Handle: handle,
	}
	repgetp := new(gfs.GetPrimaryAndSecondariesReply)
	err1 := util.Call(c.master, "Master.RPCGetPrimaryAndSecondaries", arggetp, &repgetp)
	if err1 != nil {
		log.Fatal("Client GetPrimaryAndSecondaries(Write) Error:", err1)
		log.Exit(1)
	}
	CSprimary := repgetp.Primary
	CSsecondaries := repgetp.Secondaries
	//T := repgetp.Expire
	argpush := gfs.ForwardDataArg {
		Flag:      false,
		Handle:    handle,
		Data:      data,
		ForwardTo: CSforward,
	}
	reppush := new(gfs.ForwardDataReply)
	count := 0
	flag := false
	for flag == false {
		err0 := util.Call(CSnext, "ChunkServer.RPCForwardData", argpush, &reppush)
		if err0 != nil {
			count ++
			if count >= 10 {
				goto TryAgainWrite
			}
		}
		if reppush.ErrorCode == 0 {
			flag = true
		} else {
			count ++
			if count >= 10 {
				goto TryAgainWrite
			}
		}
	}
	arg := gfs.WriteChunkArg {
		DataID:      reppush.DataID,
		Offset:      offset,
		Secondaries: CSsecondaries,
	}
	rep := new(gfs.WriteChunkReply)
	flag = false
	count = 0
	for flag == false {
		err0 := util.Call(CSprimary, "ChunkServer.RPCWriteChunk", arg, &rep)
		if err0 != nil {
			count ++
			if count >= 10 {
				goto TryAgainWrite
			}
			continue
		}
		if rep.ErrorCode == 0 {
			flag = true
		} else {
			count ++
			if count >= 10 {
				goto TryAgainWrite
			}
		}
	}
	return err
}

// AppendChunk appends data to a chunk.
// Chunk offset of the start of data will be returned if success.
// len(data) should be within max append size.
func (c *Client) AppendChunk(handle gfs.ChunkHandle, data []byte) (offset gfs.Offset, err error, errid int) {
	argreplicas := gfs.GetReplicasArg {
		Handle: handle,
	}
	//fmt.Println(data, int(handle), 0)
	//fmt.Print("")
	counttry := 0
	ret := gfs.Offset(0)
	errID := 0
	ok := false
	for ok == false {
	//fmt.Println("Here To Try Again", counttry)
	counttry ++
	repreplicas := new(gfs.GetReplicasReply)
	err0 := util.Call(c.master, "Master.RPCGetReplicas", argreplicas, &repreplicas)
	if err0 != nil {
		log.Fatal("Client Get Chunkserver(Append) Error:", err0);
		log.Exit(1)
	}
	loc := make([]string, len(repreplicas.Locations))
	for i := 0; i < len(repreplicas.Locations); i ++ {
		loc[i] = string(repreplicas.Locations[i])
	}
	sort.Strings(loc)
	for i := 0; i < len(repreplicas.Locations); i ++ {
		repreplicas.Locations[i] = gfs.ServerAddress(loc[i])
	}
	CSnext := repreplicas.Locations[0]
	CSforward := repreplicas.Locations[1:]
	//T := repgetp.Expire
	argpush := gfs.ForwardDataArg {
		Flag:      false,
		Handle:    handle,
		Data:      data,
		ForwardTo: CSforward,
	}
	reppush := new(gfs.ForwardDataReply)
	flag := false
	count := 0
	for flag == false {
		err0 := util.Call(CSnext, "ChunkServer.RPCForwardData", argpush, &reppush)
		if err0 != nil {
			//log.Fatal("Client Push Data(Append) Error:", err0)
			count ++
			if count >= 10 {
				break
			}
			continue
		}
		if reppush.ErrorCode == 0 {
			flag = true
		} else {
			count ++
			//fmt.Println("!!!!TryForward!!!!")
			if count >= 10 {
				break
			}
		}
	}
	if flag == false {
		continue
	}
	arggetp := gfs.GetPrimaryAndSecondariesArg {
		Handle: handle,
	}
	repgetp := new(gfs.GetPrimaryAndSecondariesReply)
	err1 := util.Call(c.master, "Master.RPCGetPrimaryAndSecondaries", arggetp, &repgetp)
	if err1 != nil {
		log.Fatal("Client GetPrimaryAndSecondaries(Write) Error:", err1)
		log.Exit(1)
	}
	CSprimary := repgetp.Primary
	CSsecondaries := repgetp.Secondaries
	arg := gfs.AppendChunkArg {
		DataID:      reppush.DataID,
		Secondaries: CSsecondaries,
	}
	rep := new(gfs.AppendChunkReply)
	flag = false
	count = -1
	for flag == false {
		count ++
		//fmt.Println("!!!!TryDo!!!!", count, "Primary", CSprimary, "Secondaries", CSsecondaries[0], CSsecondaries[1])
		if count >= 10 {
			break
		}
		err0 := util.Call(CSprimary, "ChunkServer.RPCAppendChunk", arg, &rep)
		if err0 != nil {
			//fmt.Println("Someone dead!!!TryAgain!!")
			continue
			//log.Fatal("Client AppendChunk Error:", err0)
		}
		if rep.ErrorCode == 0 {
			flag = true
			ok = true
			break
		}
		if rep.ErrorCode == 2 {
			flag = true
			ok = true
			break
		}
	}
	if flag == false {
		continue
	}
	ret = rep.Offset
	errID = int(rep.ErrorCode)
	}
	return ret, err, errID
}
