package chunkserver

import (
	log "github.com/Sirupsen/logrus"
	"net/rpc"
	"os"
	"gfs"
	//"gfs/util"
	"strconv"
	"sync"
	"time"
	"net"
	"gfs/util"
	"errors"
	"io/ioutil"
	//"fmt"
	//"io/ioutil"
)

// ChunkServer struct
type ChunkServer struct {
	sync.RWMutex
	address    gfs.ServerAddress // chunkserver address
	master     gfs.ServerAddress // master address
	serverRoot string            // path to data storage
	l          net.Listener
	shutdown   chan struct{}
	dead       bool              // set to true if server is shutdown

	dl                     *downloadBuffer                // expiring download buffer
	pendingLeaseExtensions *util.ArraySet                 // pending lease extension
	chunk                  map[gfs.ChunkHandle]*chunkInfo // chunk information
}

type Mutation struct {
	mtype   gfs.MutationType
	version gfs.ChunkVersion
	data    []byte
	offset  gfs.Offset
}

type chunkInfo struct {
	sync.RWMutex
	length        gfs.Offset
	version       gfs.ChunkVersion               // version number of the chunk in disk
	newestVersion gfs.ChunkVersion               // allocated newest version number
	mutations     map[gfs.ChunkVersion]*Mutation // mutation buffer
	buffers       map[gfs.DataBufferID]int64
	getbuffers    []gfs.DataBufferID
	ToMut         []int
	offsets       map[gfs.DataBufferID]gfs.Offset
	curID         int64 
	TodoID        int64
}

// NewAndServe starts a chunkserver and return the pointer to it.
func NewAndServe(addr, masterAddr gfs.ServerAddress, serverRoot string) *ChunkServer {
	cs := &ChunkServer{
		address:    addr,
		shutdown:   make(chan struct{}),
		master:     masterAddr,
		serverRoot: serverRoot,
		dl:         newDownloadBuffer(gfs.DownloadBufferExpire, gfs.DownloadBufferTick),
		pendingLeaseExtensions: new(util.ArraySet),
		chunk: make(map[gfs.ChunkHandle]*chunkInfo),
	}
	rpcs := rpc.NewServer()
	rpcs.Register(cs)
	l, e := net.Listen("tcp", string(cs.address))
	if e != nil {
		log.Fatal("listen error:", e)
		log.Exit(1)
	}
	cs.l = l

	FileName := string(cs.serverRoot) + "/ShutDownInfo"
	_, er := os.Stat(FileName)
	exit := false
	if er == nil || os.IsExist(er) {
		exit = true 
	}

	if exit {
	File, _ := os.OpenFile(FileName, os.O_RDWR, 0755)
	text, _ := ioutil.ReadFile(FileName)
	curstring := ""
	p := 0
	q := 0
	for p < len(text) {
		q = p;
		for text[q] != 44 {
			q ++
		}
		curstring = string(text[p:q])
		curCH, _ := strconv.ParseInt(curstring, 10, 64)
		ChunkFileName := string(cs.serverRoot) + "/" + curstring + "Info"
		_, errr := os.Stat(ChunkFileName)
		ex := false
		if errr == nil || os.IsExist(errr) {
			ex = true
		}
		if ex {
		ChunkFile, _ := os.OpenFile(ChunkFileName, os.O_RDWR, 0755)
		chunktext, _ := ioutil.ReadFile(ChunkFileName)
		x := 0
		y := 0
		for y = x; y < len(chunktext) && chunktext[y] != 10; y ++ {
		}
		chunkstring := string(chunktext[x:y])
		clength, _ := strconv.ParseInt(chunkstring, 10, 64)
		x = y + 1
		for y = x; y < len(chunktext) && chunktext[y] != 10; y ++ {
		}
		chunkstring = string(chunktext[x:y])
		cversion, _ := strconv.ParseInt(chunkstring, 10, 64)
		x = y + 1
		for y = x; y < len(chunktext) && chunktext[y] != 10; y ++ {
		}
		chunkstring = string(chunktext[x:y])
		cnewestversion, _ := strconv.ParseInt(chunkstring, 10, 64)
		cChunkInfo := &chunkInfo {
			length:        gfs.Offset(clength),
			version:       gfs.ChunkVersion(cversion),
			newestVersion: gfs.ChunkVersion(cnewestversion),
			mutations:     make(map[gfs.ChunkVersion]*Mutation),
			buffers:       make(map[gfs.DataBufferID]int64),
			getbuffers:    make([]gfs.DataBufferID, 0, 1),
			ToMut:         make([]int, 0, 1),
			offsets:       make(map[gfs.DataBufferID]gfs.Offset),
			curID:         0,
			TodoID:        0,
		}
		cs.chunk[gfs.ChunkHandle(curCH)] = cChunkInfo
		ChunkFile.Close()
		os.Remove(ChunkFileName)
		}
		p = q + 1
	}
	File.Close()
	os.Remove(FileName)
	}

	//shutdown := make(chan struct{})
	// RPC Handler
	go func() {
		for {
			select {
			case <-cs.shutdown:
				return
			default:
			}
			conn, err := cs.l.Accept()
			if err == nil {
				go func() {
					rpcs.ServeConn(conn)
					conn.Close()
				}()
			} else {
				// if chunk server is dead, ignores connection error
          		if !cs.dead {
            		log.Fatal(err)
            	}
			}
		}
	}()

	// Heartbeat
	go func() {
		for {
			select {
			case <-cs.shutdown:
				return
			default:
			}
			pe := cs.pendingLeaseExtensions.GetAllAndClear()
			le := make([]gfs.ChunkHandle, len(pe))
			for i, v := range pe {
				le[i] = v.(gfs.ChunkHandle)
			}
			args := &gfs.HeartbeatArg{
				Address:         addr,
				LeaseExtensions: le,
			}
			if err := util.Call(cs.master, "Master.RPCHeartbeat", args, nil); err != nil {
				log.Warningf("heartbeat rpc error ", err)
				//log.Exit(1)
			}

			time.Sleep(gfs.HeartbeatInterval)
		}
	}()

	log.Infof("ChunkServer is now running. addr = %v, root path = %v, master addr = %v", addr, serverRoot, masterAddr)

	return cs
}

// Shutdown shuts the chunkserver down
func (cs *ChunkServer) Shutdown() {
	cs.Lock()
	defer cs.Unlock()
	FileName := string(cs.serverRoot) + "/ShutDownInfo"
	File, err := os.OpenFile(FileName, os.O_RDWR|os.O_CREATE, 0755)
	if err != nil {
		log.Fatal("Shutdown Creat File Error:", err)
		log.Exit(1)
	}
	text := make([]byte, 0, 1)
	curstring := ""
	for curCH, curC := range cs.chunk {
		curC.RLock()
		curstring = strconv.FormatInt(int64(curCH), 10)
		for i := 0; i < len(curstring); i ++ {
			text = append(text, curstring[i])
		}
		text = append(text, 44)
		ChunkFileName := string(cs.serverRoot) + "/" + curstring + "Info"
		ChunkFile, _ := os.OpenFile(ChunkFileName, os.O_RDWR|os.O_CREATE, 0755)
		chunktext := make([]byte, 0, 1)
		chunkstring := strconv.FormatInt(int64(curC.length), 10)
		for i := 0; i < len(chunkstring); i ++ {
			chunktext = append(chunktext, chunkstring[i])
		}
		chunktext = append(chunktext, 10)
		chunkstring = strconv.FormatInt(int64(curC.version), 10)
		for i := 0; i < len(chunkstring); i ++ {
			chunktext = append(chunktext, chunkstring[i])
		}
		chunktext = append(chunktext, 10)
		chunkstring = strconv.FormatInt(int64(curC.newestVersion), 10)
		for i := 0; i < len(chunkstring); i ++ {
			chunktext = append(chunktext, chunkstring[i])
		}
		chunktext = append(chunktext, 10)
		_, _ = ChunkFile.WriteAt(chunktext, 0)
		ChunkFile.Close()
		curC.RUnlock()
	}
	_, _ = File.WriteAt(text, 0)
	File.Close()
	//fmt.Println("???????????????? Shutdown ????????????????????", cs.address)
	if !cs.dead {
		log.Warningf("ChunkServe %v shuts down", cs.address)
		cs.dead = true
		close(cs.shutdown)
		cs.l.Close()
	}
}

func (cs *ChunkServer) RPCReportSelf(args gfs.ReportSelfArg, reply *gfs.ReportSelfReply) error {
	cs.RLock()
	defer cs.RUnlock()
	//fmt.Println("MMMMMMMMMMMMMMM   Report   MMMMMMMMMMMMMMMMMMMM")
	//fmt.Println("Address = ", cs.address)
	for curCH, curC := range cs.chunk {
		curC.RLock()
		//fmt.Println(int64(curCH),";")
		reply.HandleList = append(reply.HandleList, curCH)
		reply.ChunkVersion = append(reply.ChunkVersion, curC.newestVersion)
		curC.RUnlock()
	}
	//fmt.Println("\nMMMMMMMMMMMMMMM   END   MMMMMMMMMMMMMMMMMMMM")
	return nil
}

// RPCForwardData is called by another replica who sends data to the current memory buffer.
// TODO: This should be replaced by a chain forwarding.
func (cs *ChunkServer) RPCForwardData(args gfs.ForwardDataArg, reply *gfs.ForwardDataReply) error {
	cs.RLock()
	defer cs.RUnlock()
	curCH := args.Handle
	cs.chunk[curCH].Lock()
	//fmt.Println("***ForwardLock:", cs.address, ":", curCH)
	curChunk := cs.chunk[curCH]
	curChunk.newestVersion ++
	curD := args.Data
	//fmt.Println(curD, string(cs.address), "Forward!")
	buffid := args.DataID
	if args.Flag == false {
		buffid = cs.dl.New(curCH)
	}
	//fmt.Println("wwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwww0")
	cs.dl.Set(buffid, curD)
	reply.DataID = buffid
	curChunk.buffers[buffid] = curChunk.curID
	curChunk.getbuffers = append(curChunk.getbuffers, buffid)
	curChunk.ToMut = append(curChunk.ToMut, -1)
	curChunk.curID ++
	//fmt.Println("***ForwardUnLock:", cs.address, ":", curCH)
	cs.chunk[curCH].Unlock()
	//fmt.Println("!!!!!!!!!!!Forward to Now!", cs.address)
	if len(args.ForwardTo) == 0 {
		return nil
	}
	//fmt.Println("wwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwww1")
	nextCS := args.ForwardTo[0]
	nextFT := args.ForwardTo[1:]
	arg := gfs.ForwardDataArg {
		Flag:      true,
		DataID:    buffid,
		Handle:    curCH,
		Data:      curD,
		ForwardTo: nextFT,
	}
	//fmt.Println("wwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwww2")
	rep := new(gfs.ForwardDataReply)
	err := util.Call(nextCS, "ChunkServer.RPCForwardData", arg, &rep)
	reply.ErrorCode = rep.ErrorCode
	if err != nil {
		reply.ErrorCode = 1
		return err
	}
	return err
}

// RPCCreateChunk is called by master to create a new chunk given the chunk handle.
func (cs *ChunkServer) RPCCreateChunk(args gfs.CreateChunkArg, reply *gfs.CreateChunkReply) error {
	cs.RLock()
	defer cs.RUnlock()
	arg := gfs.ReportSelfArg {}
	rep := new(gfs.ReportSelfReply)
	_ = util.Call(cs.address, "ChunkServer.RPCReportSelf", arg, &rep)
	//fmt.Println("#########Address:", cs.address, len(rep.HandleList))
	//for i := 0; i < len(rep.HandleList); i ++ {
	//	fmt.Print(int64(rep.HandleList[i]), ",")
	//}
//	fmt.Println()
	NewCH := args.Handle
	NewChunk := &chunkInfo {
		length:        0,
		version:       0,
		newestVersion: 0,
		mutations:     make(map[gfs.ChunkVersion]*Mutation),
		buffers:       make(map[gfs.DataBufferID]int64),
		getbuffers:    make([]gfs.DataBufferID, 0, 1),
		ToMut:         make([]int, 0, 1),
		offsets:       make(map[gfs.DataBufferID]gfs.Offset),
		curID:         0,
		TodoID:        0,
	}
	FileName := string(cs.serverRoot) + "/" + strconv.FormatInt(int64(NewCH), 10)
	_, err := os.OpenFile(FileName, os.O_RDWR|os.O_CREATE, 0755)
	if err != nil {
		log.Fatal("ChunkServer Create Chunk Error:", err)
		reply.ErrorCode = 1
		log.Exit(1)
	}
	cs.chunk[NewCH] = NewChunk
	return err
}

// RPCReadChunk is called by client, read chunk data and return
func (cs *ChunkServer) RPCReadChunk(args gfs.ReadChunkArg, reply *gfs.ReadChunkReply) error {
	cs.RLock()
	defer cs.RUnlock()
	curhandle := args.Handle
	cs.chunk[curhandle].RLock()
	//fmt.Println("***RLock:", cs.address, ":", curhandle)
	//defer fmt.Println("***UnRLock:", cs.address, ":", curhandle)
	defer cs.chunk[curhandle].RUnlock()
	L := args.Length
	off := args.Offset
	FileName := string(cs.serverRoot) + "/" + strconv.FormatInt(int64(curhandle), 10)
	File, err := os.OpenFile(FileName, os.O_RDONLY, 0755)
	if err != nil {
		log.Fatal("ChunkServer ReadChunk File Open Error:", err)
		reply.ErrorCode = 1
		log.Exit(1)
	}
	text := make([]byte, L)
	ll, err0 := File.ReadAt(text, int64(off))
	if err0 != nil {
		reply.ErrorCode = 4
	}
	File.Close()
	reply.Data = make([]byte, ll)
	reply.Data = text[0:ll]
	//fmt.Println(len(reply.Data), len(text), L, int(off))
	//fmt.Println(ll, int(off), len(reply.Data), "sever")
	reply.Length = ll
	//fmt.Println("###", "Address:", string(cs.address), "ChunkHandle:", int64(curhandle), "-->", FileName, reply.Length, "TEXT", reply.Data)
	return nil
}

// RPCWriteChunk is called by client
// applies chunk write to itself (primary) and asks secondaries to do the same.
func (cs *ChunkServer) RPCWriteChunk(args gfs.WriteChunkArg, reply *gfs.WriteChunkReply) error {
	cs.RLock()
	defer cs.RUnlock()
	curCH := args.DataID.Handle
	cs.chunk[curCH].Lock()
	//fmt.Println("***Lock:", cs.address, ":", curCH)
	//defer fmt.Println("***UnLock:", cs.address, ":", curCH)
	defer cs.chunk[curCH].Unlock()
	curChunk := cs.chunk[curCH]
	curDataID := args.DataID
	curOff := args.Offset
	Da, _ := cs.dl.Get(curDataID)
	ll := len(Da) + int(curOff)
	if ll > int(curChunk.length) {
		curChunk.length = gfs.Offset(ll)
	}
	//fmt.Println(Da, string(cs.address), int64(curOff), "1")
	myID := curChunk.buffers[curDataID]
	curChunk.offsets[curDataID] = curOff
	curChunk.ToMut[myID] = 0
	MaxID := curChunk.TodoID
	for MaxID < curChunk.curID && curChunk.ToMut[MaxID] != -1 {
		MaxID ++
	}
	V := curChunk.version
	v := curChunk.version
	//fmt.Println(myID, curChunk.TodoID)
	for i := curChunk.TodoID; i < MaxID; i++ {
		//fmt.Println(i)
		buffid := curChunk.getbuffers[i]
		D, MyLifeOk := cs.dl.Get(buffid)
		if MyLifeOk == false {
			log.Fatal("I need to continue my life!")
			log.Exit(1)
		}
		off := curChunk.offsets[buffid]
		//fmt.Println(D, string(cs.address), int64(off), "Do!")
		FileName := string(cs.serverRoot) + "/" + strconv.FormatInt(int64(curCH), 10)
		File, err := os.OpenFile(FileName, os.O_WRONLY, 0755)
		maxlength := int(off) + len(D)
		if maxlength < gfs.MaxChunkSize {
			reply.ErrorCode = 1
		}
		_, err = File.WriteAt(D, int64(off))
		if err != nil {
			log.Fatal("ChunkServer WriteChunk Write Error:", err)
			reply.ErrorCode = 1
			log.Exit(1)
		}
		curChunk.version = v + 1
		if curChunk.newestVersion < (v + 1) {
			curChunk.newestVersion = v + 1
		}
		newMut := &Mutation {
			mtype:   gfs.MutationType(curChunk.ToMut[i]),
			version: v + 1,
			data:    D,
			offset:  off,
		}
		curChunk.mutations[v + 1] = newMut
		v ++
		File.Close()
	}
	for cur := 0; cur < len(args.Secondaries); cur++ {
		v = V
		for i := curChunk.TodoID; i < MaxID; i++ {
			buffid := curChunk.getbuffers[i]
			off := curChunk.offsets[buffid]
			arg := gfs.ApplyMutationArg {
				Mtype:   gfs.MutationType(curChunk.ToMut[i]),
				Version: v,
				DataID:  buffid,
				Offset:  off,
			}
			rep := new(gfs.ApplyMutationReply)
			err := util.Call(args.Secondaries[cur], "ChunkServer.RPCApplyMutation", arg, &rep)
			reply.ErrorCode = rep.ErrorCode
			if err != nil {
				reply.ErrorCode = 1
				return err
			}
		}
	}
	curChunk.TodoID = MaxID
	return nil
}
 
// RPCAppendChunk is called by client to apply atomic record append.
// The length of data should be within max append size.
// If the chunk size after appending the data will excceed the limit,
// pad current chunk and ask the client to retry on the next chunk.
func (cs *ChunkServer) RPCAppendChunk(args gfs.AppendChunkArg, reply *gfs.AppendChunkReply) error {
	cs.RLock()
	defer cs.RUnlock()
	/*fmt.Println("++++++++++++++++++++++APPEND++++++++++++++++++")
	arg := gfs.ReportSelfArg {}
	rep := new(gfs.ReportSelfReply)
	_ = util.Call(cs.address, "ChunkServer.RPCReportSelf", arg, &rep)
	fmt.Println("#########Address:", cs.address, len(rep.HandleList))
	for i := 0; i < len(rep.HandleList); i ++ {
		fmt.Print(int64(rep.HandleList[i]), ",")
	}*/
	curCH := args.DataID.Handle
	cs.chunk[curCH].Lock()
	//fmt.Println("***Lock:", cs.address, ":", curCH)
	//defer fmt.Println("***UnLock:", cs.address, ":", curCH)
	defer cs.chunk[curCH].Unlock()
	//fmt.Println("************* Pass 0 *************")
	curDataID := args.DataID
	curChunk := cs.chunk[curCH]
	myID := curChunk.buffers[curDataID]
	S := args.Secondaries
	for i := 0; i < len(S); i ++ {
		arg0 := gfs.AliveArg {}
		rep0 := new(gfs.AliveReply)
		_ = util.Call(S[i], "ChunkServer.RPCAlive", arg0, &rep0)
		if rep0.Exist == false {
			reply.ErrorCode = 6
			curChunk.ToMut[myID] = 10
			//fmt.Println("OUTOUTOUTOUTOUTOUTOUTOUTOUT")
			return errors.New("")
		}
	}
	curlength := curChunk.length
	curdata, _ := cs.dl.Get(curDataID)
	curChunk.ToMut[myID] = 1
	flag := true
	ok := false
	reply.Offset = curlength
	/*if int(curlength) >= int(gfs.MaxChunkSize) {
		reply.ErrorCode = 2
		return nil
	}*/ 
	if int(curlength) + len(curdata) > int(gfs.MaxChunkSize) {
		//fmt.Println("OAOAOAOAO", int(gfs.MaxChunkSize) - int(curlength))
		//newdata := make([]byte, 0)
		//if int(gfs.MaxChunkSize) - int(curlength) > 0 {
		//	newdata = make([]byte, int(gfs.MaxChunkSize) - int(curlength))
		//}
		ok = true
		//for i := 0; i < len(newdata); i++ {
		//	newdata[i] = byte(10) 
		//}
		//cs.dl.Set(curDataID, newdata)
		curChunk.ToMut[myID] = 2
		flag = false
	}
	curChunk.offsets[curDataID] = curlength
	curChunk.length = curChunk.length + gfs.Offset(len(curdata))
	if curChunk.length > gfs.MaxChunkSize {
		curChunk.length = gfs.MaxChunkSize 
	}
	if ok && curChunk.length < gfs.MaxChunkSize {
		return errors.New("QwwwwwwwwwQ\n")
	}
	//fmt.Println("OAOAOAO1")
	MaxID := curChunk.TodoID
	for MaxID < curChunk.curID {
		for i := 0; i < len(S); i ++ {
			arg0 := gfs.HealthyArg {
				BuffID: curChunk.getbuffers[MaxID],
			}
			rep0 := new(gfs.HealthyReply)
			_ = util.Call(S[i], "ChunkServer.RPCHealthy", arg0, &rep0)
			if rep0.Exist == false {
				curChunk.ToMut[MaxID] = 10
			}
		}
		if curChunk.ToMut[MaxID] == -1 {
			break
		}
		MaxID ++
	}
	//fmt.Println("~~~~~~~~~~~~~~~~~~!!!!!!CS primary", myID, curChunk.TodoID, MaxID)
	//fmt.Println("************* Pass 2 *************")
	//fmt.Println("***", curCH , "MaxID :", MaxID, "begin===========")
	V := curChunk.version
	v := curChunk.version
	for i := curChunk.TodoID; i < MaxID; i++ {
	if curChunk.ToMut[i] != 10 {
		buffid := curChunk.getbuffers[i]
		D, MyLifeOk := cs.dl.Get(buffid)
		if MyLifeOk == false {
			log.Println(cs.address, "+1s!")
			log.Fatal("Append:I need to continue my life!")
			log.Exit(1)
		}
		//fmt.Println("!!!!!!CS primary", cs.address, D)
		//fmt.Println("*** Pass 0 ***", i)
		off := curChunk.offsets[buffid]
		//fmt.Println(D, string(cs.address), int64(off), int(curCH), "Do!")
		FileName := string(cs.serverRoot) + "/" + strconv.FormatInt(int64(curCH), 10)
		File, err := os.OpenFile(FileName, os.O_WRONLY, 0755)
		//fmt.Println("*** Pass 1 ***", i, "len:", len(D), "off:", int(off))
		if int(off) + len(D) > int(gfs.MaxChunkSize) {
			D = D[0:int(gfs.MaxChunkSize) - int(off)]
		}
		//fmt.Println("*** Pass 2 ***", i, "Data:", D)
		_, err = File.WriteAt(D, int64(off))
		//fmt.Println("!!!Address:", string(cs.address), curCH, curdata, curlength, "appendchunk")
		if err != nil {
			log.Fatal("ChunkServer AppendChunk Write Error:", err)
			reply.ErrorCode = 1
			log.Exit(1)
		}
		curChunk.version = v + 1
		if curChunk.newestVersion < (v + 1) {
			curChunk.newestVersion = v + 1
		}
		newMut := &Mutation {
			mtype:   gfs.MutationType(curChunk.ToMut[i]),
			version: v + 1,
			data:    D,
			offset:  off,
		}
		curChunk.mutations[v + 1] = newMut
		v ++
		File.Close()
	}
	}
	//fmt.Println("************* Pass 3 *************")
	for cur := 0; cur < len(args.Secondaries); cur++ {
		v = V
		for i := curChunk.TodoID; i <  MaxID; i++ {
			if curChunk.ToMut[i] != 10 {
			buffid := curChunk.getbuffers[i]
			off := curChunk.offsets[buffid]
			arg := gfs.ApplyMutationArg {
				Mtype:   gfs.MutationType(curChunk.ToMut[i]),
				Version: v,
				DataID:  buffid,
				Offset:  off,
			}
			rep := new(gfs.ApplyMutationReply)
			err := util.Call(args.Secondaries[cur], "ChunkServer.RPCApplyMutation", arg, &rep)
			if err != nil {
				//fmt.Printf("I'm dead!!", args.Secondaries[cur])
				reply.ErrorCode = 1
				return err
			}
			if err == nil && reply.ErrorCode == 6 {
				continue
			}
			reply.ErrorCode = rep.ErrorCode
		}
		}
	}
	if flag == false {
		reply.ErrorCode = 2
	}
	//fmt.Println("************* Pass 4 *************")
	curChunk.TodoID = MaxID
	//fmt.Println("***", curCH, "TodoID:", curChunk.TodoID, "end  ===========")
	return nil
}

// RPCApplyWriteChunk is called by primary to apply mutations
func (cs *ChunkServer) RPCApplyMutation(args gfs.ApplyMutationArg, reply *gfs.ApplyMutationReply) error {
	cs.RLock()
	defer cs.RUnlock()
	/*fmt.Println("++++++++++++++++++++++APPEND++++++++++++++++++")
	arg := gfs.ReportSelfArg {}
	rep := new(gfs.ReportSelfReply)
	_ = util.Call(cs.address, "ChunkServer.RPCReportSelf", arg, &rep)
	fmt.Println("#########Address:", cs.address, len(rep.HandleList))
	for i := 0; i < len(rep.HandleList); i ++ {
		fmt.Print(int64(rep.HandleList[i]), ",")
	}*/
	buffid := args.DataID
	_, ok := cs.dl.Get(buffid)
	if ok == false {
		reply.ErrorCode = 6
		return nil
	}
		curCH := buffid.Handle
		cs.chunk[curCH].Lock()
		//fmt.Println("***Lock:", cs.address, ":", curCH)
		//defer fmt.Println("***UnLock:", cs.address, ":", curCH)
		defer cs.chunk[curCH].Unlock()
		myID := cs.chunk[curCH].buffers[buffid]
		cs.chunk[curCH].ToMut[myID] = int(args.Mtype)
		V := args.Version
		D, MyLifeOk := cs.dl.Get(buffid)
		if MyLifeOk == false {
			log.Println(cs.address, "+1s!")
			log.Fatal("ApplyMutation:I need to continue my life!")
			log.Exit(1)
		}
		//fmt.Println("!!!!!!CS secondaries", cs.address, D)
		off := args.Offset
		//fmt.Println(D, string(cs.address), int64(off), int(curCH), "Do!")
		FileName := string(cs.serverRoot) + "/" + strconv.FormatInt(int64(curCH), 10)
		File, err := os.OpenFile(FileName, os.O_WRONLY, 0755)
		if err != nil {
			log.Fatal("ChunkServer ApplyMutation File Open Error:", err)
			reply.ErrorCode = 1
			log.Exit(1)
		}
		if int(off) + len(D) > int(gfs.MaxChunkSize) {
			D = D[0:int(gfs.MaxChunkSize) - int(off)]
		}
		_, err = File.WriteAt(D, int64(off))
		if err != nil {
			log.Fatal("ChunkServer ApplyMutation Write Error:", err)
			reply.ErrorCode = 1
			log.Exit(1)
		}
		cs.chunk[curCH].version = V + 1
		if cs.chunk[curCH].newestVersion < (V + 1) {
			cs.chunk[curCH].newestVersion = V + 1
		}
		curChunk := cs.chunk[curCH]
		ll := len(D) + int(off)
		if int(curChunk.length) < ll {
			curChunk.length = gfs.Offset(ll)
		}
		newMut := &Mutation {
			mtype:   gfs.MutationType(args.Mtype),
			version: V + 1,
			data:    D,
			offset:  off,
		}
		curChunk.mutations[V + 1] = newMut
		File.Close()
	return err
}

// RPCSendCCopy is called by master, send the whole copy to given address
func (cs *ChunkServer) RPCSendCopy(args gfs.SendCopyArg, reply *gfs.SendCopyReply) error {
	cs.RLock()
	defer cs.RUnlock()
	ToCS := args.Address
	curCH := args.Handle
	cs.chunk[curCH].RLock()
	defer cs.chunk[curCH].RUnlock()
	FileName := string(cs.serverRoot) + "/" + strconv.FormatInt(int64(curCH), 10)
	File, err := os.OpenFile(FileName, os.O_RDONLY, 0755)
	if err != nil {
		log.Fatal("ChunkServer SendCopy File Open Error:", err)
		reply.ErrorCode = 5
		log.Exit(1)
	}
	text, _ := ioutil.ReadFile(FileName)
	if err != nil {
		log.Fatal("ChunkServer SendCopy Read Text Error:", err)
		reply.ErrorCode = 5
		log.Exit(1)
	}
	ver := cs.chunk[curCH].version
	arg := gfs.ApplyCopyArg {
		Handle:  curCH,
		Data:    text,
		Version: ver,
	}
	rep := new(gfs.ApplyCopyReply)
	err1 := util.Call(ToCS, "ChunkServer.RPCApplyCopy", arg, &rep)
	if err1 != nil {
		reply.ErrorCode = 2
		return err1
	}
	File.Close()
	return err1
}

// RPCSendCCopy is called by another replica
// rewrite the local version to given copy data
func (cs *ChunkServer) RPCApplyCopy(args gfs.ApplyCopyArg, reply *gfs.ApplyCopyReply) error {
	cs.RLock()
	defer cs.RUnlock()
	NewCH := args.Handle
	L := len(args.Data)
	V := args.Version
	NewChunk := &chunkInfo {
		length:        gfs.Offset(L),
		version:       V,
		newestVersion: V,
		mutations:     make(map[gfs.ChunkVersion]*Mutation),
		buffers:       make(map[gfs.DataBufferID]int64),
		getbuffers:    make([]gfs.DataBufferID, 0, 1),
		ToMut:         make([]int, 0, 1),
		offsets:       make(map[gfs.DataBufferID]gfs.Offset),
		curID:         0,
		TodoID:        0,
 	}
	FileName := string(cs.serverRoot) + "/" + strconv.FormatInt(int64(NewCH), 10)
	File, err := os.OpenFile(FileName, os.O_RDWR|os.O_CREATE, 0755)
	if err != nil {
		log.Fatal("ChunkServer ApplyCopy Create Chunk Error:", err)
		reply.ErrorCode = 1
		log.Exit(1)
	}
	cs.chunk[NewCH] = NewChunk
	cs.chunk[NewCH].Lock()
	defer cs.chunk[NewCH].Unlock()
	_, err = File.WriteAt(args.Data, 0)
	if err != nil {
		log.Fatal("ChunkServer ApplyCopy Write Error:", err)
		reply.ErrorCode = 1
		log.Exit(1)
	}
	File.Close()
	return err
}  

func (cs *ChunkServer) RPCAlive(args gfs.AliveArg, reply *gfs.AliveReply) error {
	if cs.dead == true {
		reply.Exist = false
	} else {
		reply.Exist = true
	}
	return nil
}

func (cs *ChunkServer) RPCHealthy(args gfs.HealthyArg, reply *gfs.HealthyReply) error {
	_, ok := cs.dl.Get(args.BuffID)
	reply.Exist = ok
	return nil
}